/**
 * Copyright (C) taqql, inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by alantoo <x.alantoo@gmail.com>, 7/3/17
 */

const webpack = require("webpack");
const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const theme = require("./theme");

module.exports = {
  entry: [
    "babel-polyfill",
    // "react-hot-loader/patch",
    "./src/app.tsx"
  ],
  output: {
    filename: "assets/js/bundle.min.js",
    publicPath: "/",
    path: path.join(__dirname, "/public/"),
  },

  devtool: "eval-source-map",
  // devtool: false,
  cache: true,
  bail: false,
  devServer: {
    // hot: true,
    //inline: true,
    stats: {
      progress: true,
      colors: true,
    },
    port: 8085,
    historyApiFallback: {
      index: "index.html",
    },
    contentBase: path.join(__dirname, "public"),
    host: "127.0.0.1",
  },

  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"],
    modules: ["src", "node_modules"],
    alias: {
      // "actions": path.resolve(__dirname, "./src/actions"),
      "components": path.resolve(__dirname, "./src/components"),
      // "constants": path.resolve(__dirname, "./src/constants"),
      // "containers": path.resolve(__dirname, "./src/containers"),
      // "models": path.resolve(__dirname, "./src/models"),
      // "modules": path.resolve(__dirname, "./src/modules"),
      // "screens": path.resolve(__dirname, "./src/screens"),
      // "pages": path.resolve(__dirname, "./src/pages"),
      // "reducers": path.resolve(__dirname, "./src/reducers"),
      // "global-styles": path.resolve(__dirname, "./src/global-styles"),
      // "vars": path.resolve(__dirname, "./src/global-styles/vars"),
      // "services": path.resolve(__dirname, "./src/services"),
      // "utils": path.resolve(__dirname, "./src/utils"),
    },
  },

  module: {
    rules: [
       {
        test: /\.tsx?$/,
        loaders: "eslint-loader",
        enforce: "pre",
        include: path.resolve("src"),
      },
      {
        test: /\.tsx?$/,
        loaders: ["babel-loader?cacheDirectory=babel_cache", "ts-loader?transpileOnly=true"],
        include: path.resolve("src"),
      },
      {
        test: /\.scss$/,
        use: ["css-hot-loader"].concat(ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader?modules=true&sourceMap=true", "sass-loader"],
        })),
      },
      {
        test: /\.less$/,
        use: ["css-hot-loader"].concat(ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader?{\"modules\": false, \"sourceMap\": true}", `less-loader?{"modifyVars":${JSON.stringify(theme)}}`],
        })),
        // test: /\.less$/,
        // loaders: ["css-loader", "less-loader"]
      },
      {
        test: /\.css$/,
        use: ["css-hot-loader"].concat(ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            `css-loader?{"modules": true, "sourceMap": true}`
          ],
        })),
      },
      {
        test: /\.(png|jpe?g|svg|gif)$/,
        use: {
          loader: "url-loader",
          options: {
            pageSize: 15000,
            name: "[treeNodeName].[hash].[ext]",
          },
        },
      }
    ],
  },
  plugins: [
    // new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin("assets/css/bundle.min.css")
  ],
};
