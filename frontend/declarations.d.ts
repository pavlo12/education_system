declare module "react-hot-loader";
declare module "react-router-redux";
declare module "resolff-file-picker";
declare module "ramda";

declare module "keymirror" {
	function keyMirror(obj: Object): any;

	export = keyMirror;
}

declare module "*.png" {
	const value: any;
	export = value;
}
declare module "*.jpg" {
	const value: any;
	export = value;
}
declare module "*.jpeg" {
	const value: any;
	export = value;
}
declare module "*.gif" {
	const value: any;
	export = value;
}
declare module "*.svg" {
	const value: any;
	export = value;
}
declare module "*.scss" {
	const value: any;
	export = value;
}
declare module "*.json" {
	const value: any;
	export = value;
}

