/**
 * Copyright (C) taqql, inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by alantoo <x.alantoo@gmail.com>, 7/7/17
 */


module.exports = {
	'primary-color': '#1DA57A'
};