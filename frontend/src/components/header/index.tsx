import React from "react";
import { Avatar, Popover, Row, Layout, Menu, Icon, } from "antd";
import ownStyles from "./styles.scss";
import { UserSetingModalWindow, } from "../user-setting";
import { constants, } from "../constants";
import { AuthService, } from "../../services";
import { DI, } from "../../di.config";


interface ILayoutHeaderState {
  isPopover: boolean,
  isOpenModal: boolean,
}

enum HeaderMenuKeys {
  PROFILE = "PROFILE",
  LOG_OUT = "LOG_OUT",
}

export class LayoutHeader extends React.PureComponent<any, ILayoutHeaderState> {

  private readonly authService: AuthService = DI.resolve(AuthService);

  state = {
    isPopover: false,
    isOpenModal: false,
  };


  handleOnClickMenu(key) {
    if (key === HeaderMenuKeys.LOG_OUT) {
        this.authService.clearAuthInfo();
        location.assign(constants.UNAUTHORIZED_REDIRECT_PATH);
        return
    }
    if (key === HeaderMenuKeys.PROFILE) {
      this.setState({
        isOpenModal: true,
      })
    }
    this.setState({
      isPopover: false,
    })
  }

  render() {

    const userAvatar = undefined;

    return (
      <div>
        <Layout.Header className={ownStyles.header}>
          <Row type="flex" justify="end">
            <Popover
              placement="leftBottom"
              overlayClassName={ownStyles.popoverItem}
              visible={this.state.isPopover}
              content={<Menu style={{ backgroundColor: "#f5f5f0", }}
                             onClick={(menuItem) => this.handleOnClickMenu(menuItem.key)}>
                <Menu.Item key={HeaderMenuKeys.PROFILE}>
                  <Icon type="idcard"/>
                  Налаштування профілю
                </Menu.Item>
                <Menu.Item key={HeaderMenuKeys.LOG_OUT}>
                  <Icon type="poweroff"/>
                  Завершити роботу
                </Menu.Item>
              </Menu>}
            >
              <div onClick={() => this.setState({
                isPopover: !this.state.isPopover,
              })}>
                <Avatar
                  src={userAvatar}
                  className={` ${ownStyles.shadow} ${userAvatar ? ownStyles.isAvatar : ownStyles.avatarItem}`}
                >A</Avatar>
              </div>
            </Popover>
          </Row>
        </Layout.Header>
        <UserSetingModalWindow
          isOpen={this.state.isOpenModal}
          closeWindow={() => {
            this.setState({
              isOpenModal: false,
            })
          }}
        />
      </div>
    )
  }

}