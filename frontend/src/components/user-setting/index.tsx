import React from "react";
import {
  Button, Col, DatePicker, Form, Input, Popconfirm, Row, Tooltip, Radio, Modal,
} from "antd";
import FormItem from "antd/es/form/FormItem";
import styles from "./styles.scss"
import avatar from "../../assets/empty-avatar.jpg"
import { WrappedFormUtils, } from "antd/es/form/Form";
import { UserService, } from "../../services/api/User.service";
import { DI, } from "../../di.config";
import { AuthService, } from "../../services";
import { IUser, } from "../../models/user";
import moment from "moment";

enum FullMaleTypes {
  MAN = "MAN",
  WOMAN = "WOMAN",
}

interface IUserSettingModalProps {
  form?: WrappedFormUtils,
  closeWindow: () => void,
  isOpen: boolean,
}

interface IUserSettingModalState {
  user: IUser,
}

class UserSetingModalWindow extends React.PureComponent<IUserSettingModalProps, any> {

  private readonly userService: UserService = DI.resolve(UserService);
  private readonly authService: AuthService = DI.resolve(AuthService);

  state = {
    user: undefined as IUser,
  };

  componentDidMount() {
    this.getUser();
  }

  getUser() {
    this.userService.getOne(this.authService.getLogin()).then((response: IUser) => {
      this.setState({
        user: response,
      })
    }).catch(err => {
      console.dir(err)
    })
  }

  handleOnSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      const user: IUser = {
        ...fieldsValue,
        version: this.state.user.version,
        birthDate: fieldsValue.birthDate.valueOf(),
        id: this.authService.getLogin(),
      };

      this.userService.update(user).then((response: IUser) => {
        this.setState({
          user: response,
        })
      }).catch(err => {
        console.error(err);
      })
    });
  };

  handleOnCancel = () => {
    this.props.closeWindow();
  };

  render() {

    const { getFieldDecorator, } = this.props.form;
    const { user, } = this.state;

    return (
      <Modal
        title={"Налаштування профілю"}
        visible={this.props.isOpen}
        width={850}
        closable
        // onCancel={onCancel}
        footer={null}
      >
        <Form
          onSubmit={this.handleOnSubmit}
        >
          <Row gutter={10}>
            <Col span={8}
              className={styles.colImageStyles}
            >
              <div>
                <div className={styles.countryImage}>
                  <img
                    className={styles.preview}
                    src={avatar}
                  />
                  <input
                    type="file"
                    title={"Виберіть зображення"}
                    id={"inputFile"}
                  />
                </div>
              </div>
            </Col>

            <Col span={8}
              className={styles.colStyles}
            >
              <Row gutter={10}>
                <Col sm={24}>
                  <Form.Item label="Email">
                    {this.props.form.getFieldDecorator("email", {
                      rules: [
                        {
                          required: true,
                          message: "Email є обов'язковим полем",
                        }
                      ],
                      initialValue: user && user.email,
                    })(
                      <Input
                        size="small"
                        placeholder="Введіть email"
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col sm={24}>
                  <FormItem
                    label="Дата народження"
                  >
                    {getFieldDecorator("birthDate",  {
                      rules: [
                        {required: true, message: "Оберіть дату народження", }
                      ],
                      initialValue: (user && user.birthDate) && moment(user.birthDate),
                    })(
                      <DatePicker
                        size="small"
                        placeholder="Оберіть дату"
                        format="YYYY-MM-DD"
                      />
                    )}
                  </FormItem>
                </Col>
                <Col sm={24}>
                  <FormItem
                    label="Стать"
                  >
                    {getFieldDecorator("sex", {
                      rules: [
                        {required: true, message: "Виберіть стать", }
                      ],
                      initialValue: user && user.sex,
                    })(
                      <Radio.Group
                        size={"small"}
                      >
                        <Radio.Button value={FullMaleTypes.MAN}>Чоловік</Radio.Button>
                        <Radio.Button value={FullMaleTypes.WOMAN}>Жінка</Radio.Button>
                      </Radio.Group>)}
                  </FormItem>
                </Col>
              </Row>
            </Col>

            <Col span={8}
              className={styles.colStyles}
            >
              <Row gutter={10}>
                <Col sm={24}>
                  <Form.Item label="Ім'я">
                    {this.props.form.getFieldDecorator("firstName", {
                      rules: [
                        { required: true, message: "Ім'я є обов'язковим полем", }
                      ],
                      initialValue: user && user.firstName,
                    })(
                      <Input
                        size="small"
                        // disabled={disable}
                        placeholder="Введіть ім'я"
                      />
                    )}
                  </Form.Item>
                </Col>

                <Col sm={24}>
                  <Form.Item label="По-батькові">
                    {this.props.form.getFieldDecorator("middleName", {
                      rules: [
                        { required: true, message: "По-батькові є обов'язковим полем", }
                      ],
                      initialValue: user && user.middleName,
                    })(
                      <Input
                        // disabled={disable}
                        size="small"
                        placeholder="Введіть по-батькові"
                      />
                    )}
                  </Form.Item>
                </Col>

                <Col sm={24}>
                  <Form.Item label="Прізвище">
                    {this.props.form.getFieldDecorator("lastName", {
                      rules: [
                        { required: true, message: "Прізвище є обов'язковим полем", }
                      ],
                      initialValue: user && user.lastName,
                    })(
                      <Input
                        // disabled={disable}
                        size="small"
                        placeholder="Введіть прізвище"
                      />
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row type={"flex"} justify={"end"}>
            <Col span={8}>
              <Button.Group style={{ float: "right", width: "100%", }}>
                <Button
                  htmlType="submit"
                  type="primary"
                  size="small"
                  style={{ width: "50%", }}
                >
                  Зберегти
                </Button>
                <Popconfirm
                  title="Дійсно скасувати зміни?"
                  onConfirm={this.handleOnCancel}
                  okText="Так"
                  cancelText="Ні"
                >
                  <Tooltip placement="right" title="Скасувати зміни">
                    <Button
                      style={{ width: "50%", }}
                      ghost
                      size="small"
                      icon="cancel"
                      type="primary"
                    >
                      Скасувати
                    </Button>
                  </Tooltip>
                </Popconfirm>
              </Button.Group>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}

const UserEditFormComponentWrapped = Form.create<any>()(UserSetingModalWindow as any);

export { UserEditFormComponentWrapped as UserSetingModalWindow, };