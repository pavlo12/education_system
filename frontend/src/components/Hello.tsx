import React from "react";
import { Button, } from "antd";
import { PathsService, } from "../services";
import { DI, } from "../di.config";

export class Hello extends React.PureComponent<any, any> {

  private _pathService: PathsService = DI.resolve(PathsService);

  render() {
    return(
      <div>
           <h1>Hello</h1>
            <h3>ApiPath: {this._pathService.toAPIFullPath("")}</h3>
           <Button onClick={() => alert("Hello!!!")}>
             Hello
           </Button>
      </div>
    )
  }

}