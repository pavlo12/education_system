import React from "react";
import { Button, Form, Icon, Input, message, Spin, } from "antd";
import { WrappedFormUtils, } from "antd/es/form/Form";
import ownStyle from "./styles.scss";
import { AuthService, NotificationService, } from "../../services";
import { RequestGetToken, } from "../../services/Auth.service";
import { DI, } from "../../di.config";
import { IAuthorizationInputData, } from "../../models/authorization";


interface ILoginPageForm {
  form: WrappedFormUtils,
  history: any,
}

interface ILoginPageState {
  isSpinner: boolean,
}

class LoginPage extends React.PureComponent<ILoginPageForm, ILoginPageState> {

  private authService: AuthService = DI.resolve(AuthService);
  private getTokenRequest: RequestGetToken = DI.resolve(RequestGetToken);
  private notificationService: NotificationService = DI.resolve(NotificationService);

  state = {
    isSpinner: false,
  };

  handleOnSubmit = (event) => {

    event.preventDefault();
    const { validateFields, } = this.props.form;

    validateFields( async (err: any, fieldsValue: any) => {
      if (err !== null && err.username) {
        let text: string = "Пошта користувача є обов'язковим полем!";
        this.notificationService.showMessageWithCustomText(text);
      } else if (err !== null && err.password) {
        let text: string = "Пароль користувача є обов'язковим полем!";
        this.notificationService.showMessageWithCustomText(text);
      } else {

        this.setState({
          isSpinner: true,
        });
        const userAuthorizationData: IAuthorizationInputData = {
          username: fieldsValue["username"],
          password: fieldsValue["password"],
        };
        const response = await this.getTokenRequest.getToken(userAuthorizationData).catch(err => {
          if(!err.status) {
            message.error("Невідома помилка!")
          }
          this.setState({
            isSpinner: false,
          });
          return err;
        });
        if(response.status === 401) {
          message.error("Неправельне імя користувача або пароль");
          this.setState({
            isSpinner: false,
          });
          return;
        }
        const responseJson = await response.json();
        if(responseJson) {
          await this.authService.provider(responseJson);
          await this.authService.checkExpiredTokenTime();
        }
        this.setState({
          isSpinner: false,
        });
        this.props.history.replace("/");
      }
    });
  };

  render() {

    const {getFieldDecorator, } = this.props.form;

    return(
      <div className={ownStyle.authorizationContainer}>
        <div>
          <Spin spinning={this.state.isSpinner}>
            <Form onSubmit={this.handleOnSubmit} className={ownStyle.formWrapper}>
              <Form.Item className={ownStyle.formItem}>
                {getFieldDecorator("username", {
                  rules: [{required: true, message: "Введіть пошту користувача!", }],
                })(
                  <Input prefix={<Icon type="user" style={{fontSize: 13, }}/>}
                         placeholder="Пошта користувача"/>
                )}
              </Form.Item>
              <Form.Item className={ownStyle.formItem}>
                {getFieldDecorator("password", {
                  rules: [{required: true, message: "Введіть пароль користувача!", }],
                })(
                  <Input prefix={<Icon type="lock" style={{fontSize: 13, }}/>} type="password"
                         placeholder="Пароль користувача"/>
                )}
              </Form.Item>
              <Button icon="enter" type="primary" htmlType="submit" style={{width: "100%", }}>
                Увійти
              </Button>
            </Form>
          </Spin>
        </div>
      </div>
    )
  }
}

const LoginPageFormComponentWrapped = Form.create<any>()(LoginPage) as any;

export { LoginPageFormComponentWrapped as LoginPage, };