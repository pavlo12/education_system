const UNAUTHORIZED_REDIRECT_PATH: string = "/login";
const DELAY_TO_REFRESH_TOKEN_AGAIN: number = 15000;
const MAX_TIME_FOR_SET_TIMEOUT: number = 2147483647;

export const constants = {
 UNAUTHORIZED_REDIRECT_PATH,
  DELAY_TO_REFRESH_TOKEN_AGAIN,
  MAX_TIME_FOR_SET_TIMEOUT,
};