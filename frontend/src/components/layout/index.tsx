import { Layout, Menu, LocaleProvider, } from "antd";
import React from "react";
import pathToRegexp from "path-to-regexp";
import { Link, Redirect, Switch, } from "react-router-dom";
import { ILinkedPanelRoute, IPanelRoute, } from "../../route/Root.router";
import { routesToChildren, } from "./children";
import { LayoutHeader, } from "../header";
import styles from "./styles.scss";
import uaUA from "resources/ant/localization/ua_UA";

const {Sider, } = Layout;


interface IPublicLayoutProps {
  routes: IPanelRoute[],
}

export class PublicLayout extends React.Component<IPublicLayoutProps, any> {
  state = {
    collapsed: false,
  };

  private getRoutePath = (route: IPanelRoute, parents: IPanelRoute[], area: string): string => {
    return [...parents.map(r => r.path), route.path].join("/")
  };

  resolveRouteBuiltPath = (route: IPanelRoute): string | null => route.inNavMenuVisible ? route.path : null;

  getBuiltRoutePath = (route: IPanelRoute, parents: IPanelRoute[], area: string): string => {
    return [
      area,
      ...parents.map(r => this.resolveRouteBuiltPath(r)),
      this.resolveRouteBuiltPath(route)
    ].join("/");
  };

  linkRoutes =  (linkRoutes: any, routes: IPanelRoute[], area: string, parents: IPanelRoute[] = [], parent: any = null): ILinkedPanelRoute[] => {

    let out: ILinkedPanelRoute[] = [];

    routes.map(route => {
      const prepared: ILinkedPanelRoute = {
        ...route,
        fullpath: this.getRoutePath(route, parents, area),
        builtFullRoute: this.getBuiltRoutePath(route, parents, area),
        parent: parent,
      };

      out.push(prepared);

      if (route.children && route.children.length) {
        out = [
          ...out,
          ...linkRoutes(linkRoutes, route.children, area, [...parents, route], prepared)
        ];
      }
    });
    return out;
  };

  buildUrl = (route: ILinkedPanelRoute) => {
    const path = pathToRegexp.compile(route.path);
    const url = "/" + path(route.defaultParams || {});

    return (url);
  };

  getMenuItems = (routes: ILinkedPanelRoute[]): JSX.Element[] => {

    let menuItems: JSX.Element[] = [];
    let positions: number[] = [];



    const isSubMenu: ILinkedPanelRoute[] = routes.filter((item: IPanelRoute) => item.subMenu);


    isSubMenu.map((item: ILinkedPanelRoute | any): void => {
      let positionNumber = item.subMenu.position;
      if (positions.length === 0) {
        positions.push(positionNumber)
      } else if (positionNumber !== positions[positions.length - 1]) {
        positions.push(positionNumber)
      } else {
        return
      }
    });


    // push items WITHOUT subMenu
    routes.forEach((route: ILinkedPanelRoute): void => {
      if (route.inNavMenuVisible && !route.subMenu) {
        const url = this.buildUrl(route);
        menuItems.push(
          <Menu.Item key={route.fullpath}>
            <Link to={url}>
              {route.icon}
              <span>{route.title}</span>
            </Link>
          </Menu.Item>
        );
      }
    });

    // push items WITH subMenu
    for (let i: number = 0; i < positions.length; i++) {

      const subMenuItems: ILinkedPanelRoute[] = routes.filter((item) => {
        return item.inNavMenuVisible && item.subMenu && item.subMenu.position === positions[i]
      });

      if(subMenuItems.length < 1){
        continue;
      }
      let icon: JSX.Element = subMenuItems[0].subMenu.icon;
      let title: string = subMenuItems[0].subMenu.title;
      let position: number = subMenuItems[0].subMenu.position;

      const SubMenu: JSX.Element = (
        <Menu.SubMenu   key={i}  title={<span>{icon}{title}</span>}>
          {
            subMenuItems.map((item: ILinkedPanelRoute) => {
              const url = this.buildUrl(item);
              return (
                <Menu.Item key={item.fullpath}>
                  <Link to={url}>
                    {item.icon ? item.icon : null}
                    <span>{item.title}</span>
                  </Link>
                </Menu.Item>
              );
            })
          }
        </Menu.SubMenu>
      );

      menuItems.splice(position, 0, SubMenu)
    }
    return menuItems;
  };

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed, });
  };

  render() {

    const children = routesToChildren(this.linkRoutes(this.linkRoutes, this.props.routes, "")).map((item) => item);
    console.log(children);

    return (
      <LocaleProvider locale={uaUA}>
      <Layout style={{ minHeight: "100vh", }}>
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <div className="logo" />
          <Menu
            theme="dark"
            defaultSelectedKeys={["0"]}
            mode="inline">
            {this.getMenuItems(this.linkRoutes(this.linkRoutes, this.props.routes, ""))}
            {/*<Menu.Item key="1">*/}
              {/*<Icon type="pie-chart" />*/}
              {/*<span>Option 1</span>*/}
            {/*</Menu.Item>*/}
            {/*<Menu.Item key="2">*/}
              {/*<Icon type="desktop" />*/}
              {/*<span>Option 2</span>*/}
            {/*</Menu.Item>*/}
            {/*<SubMenu*/}
              {/*key="sub1"*/}
              {/*title={<span><Icon type="user" /><span>User</span></span>}*/}
            {/*>*/}
              {/*<Menu.Item key="3">Tom</Menu.Item>*/}
              {/*<Menu.Item key="4">Bill</Menu.Item>*/}
              {/*<Menu.Item key="5">Alex</Menu.Item>*/}
            {/*</SubMenu>*/}
            {/*<SubMenu*/}
              {/*key="sub2"*/}
              {/*title={<span><Icon type="team" /><span>Team</span></span>}*/}
            {/*>*/}
              {/*<Menu.Item key="6">Team 1</Menu.Item>*/}
              {/*<Menu.Item key="8">Team 2</Menu.Item>*/}
            {/*</SubMenu>*/}
            {/*<Menu.Item key="9">*/}
              {/*<Icon type="file" />*/}
              {/*<span>File</span>*/}
            {/*</Menu.Item>*/}
          </Menu>
        </Sider>
        <Layout className={styles.contentContainer}>
          {/*<Header style={{ background: "#fff", padding: 0, }} />*/}
          <LayoutHeader/>
          <Layout className={styles.childrenWrapper}>
            <Redirect
              exact
              from="/"
              to={"hello"}
            />
            <Switch>
              {
                children
              }
            </Switch>
          </Layout>
          {/*<Footer style={{ textAlign: "center", }}>*/}
            {/*Ant Design ©2018 Created by Ant UED*/}
          {/*</Footer>*/}
        </Layout>
      </Layout>
      </LocaleProvider>
    );
  }
}