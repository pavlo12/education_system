import React from "react";
import { Route, } from "react-router";
import { Link, } from "react-router-dom";
import { Breadcrumb, Layout, } from "antd";
// components
import ownStyles from "./styles.scss";
import { ILinkedPanelRoute, } from "../../../route/Root.router";


const getBreadcrumbsItems = (route: ILinkedPanelRoute | undefined): ILinkedPanelRoute[] => {

	let out: ILinkedPanelRoute[] = [];

	if (!route) {
		return []
	}

	if (route.parent) {
		out = [
			...getBreadcrumbsItems(route.parent)
		];
	}

	out.push(route);

	return out;
};


const getBreadcrumbs = (route: ILinkedPanelRoute): JSX.Element => {

	const flat = getBreadcrumbsItems(route.parent);

	return (
		<Breadcrumb>

			<Breadcrumb.Item>
				<Link to={"/"}>Головна</Link>
			</Breadcrumb.Item>

			{flat.map((route) => (
				<Breadcrumb.Item key={route.builtFullRoute}>
					<Link to={route.builtFullRoute}>{route.title}</Link>
				</Breadcrumb.Item>
			))}

			<Breadcrumb.Item>{route.title}</Breadcrumb.Item>

		</Breadcrumb>
	);
};

export const routesToChildren = (routes: ILinkedPanelRoute[]): JSX.Element[] => {
	let children: JSX.Element[] = [];

	routes.map((route, i) => {

		const Cmpnt: React.ComponentClass = route.component;

		children.push(
			<Route
				exact
				key={`/${route.fullpath}`}
				path={`/${route.fullpath}`}
				component={(props: any) => {
					return (
						<div className={ownStyles.container}>
							<div className={ownStyles.breadcrumbsWrapper}>
								{getBreadcrumbs(route)}
							</div>
							<Layout.Content className={ownStyles.childrenWrapper}>
								<Cmpnt  {...props}/>
							</Layout.Content>
						</div>
					)
				}}
			/>
		);
	});

	return children
};