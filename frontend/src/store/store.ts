/**
 * Copyright (C) taqql, inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by alantoo <x.alantoo@gmail.com>, 7/5/17
 */

import {
	createStore,
	applyMiddleware,
} from "redux";
import { composeWithDevTools, } from "redux-devtools-extension";
import { createLogger, } from "redux-logger";

import rootReducer from "reducers";

const loggerMiddleware = createLogger();



export default function configureStore(initState?: any) {
	return createStore(
		rootReducer,
		initState,
		composeWithDevTools(
			applyMiddleware(
				// thunkMiddleware,
				loggerMiddleware
			)
		)
	);
}
