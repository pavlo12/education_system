interface IUser {
  version: number,
  id: string,
  email: string,
  firstName: string,
  middleName: string,
  lastName: string,
  photo: string,
  birthDate: number,
  sex: string,
  password: string,
  roles: string[],
}

enum Sex {
  MAN="MAN",
  WOMAN="WOMAN",
}

export {
  IUser,
  Sex,
}