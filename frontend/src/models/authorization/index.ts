enum StoreKeys {
  IDENTITY_STORE_KEY = "USER_IDENTITY",
  RAW_TOKEN = "RAW_USER_TOKEN",
  RAW_REFRESH_TOKEN = "RAW_REFRESH_TOKEN",
  RAW_CURRENT_TOKEN = "RAW_CURRENT_TOKEN",
  DELAY_TO_SEND_REFRESH_TOKEN = "DELAY_TO_SEND_REFRESH_TOKEN",
  DATE_EXPIRE_TIME_TOKEN = "DATE_EXPIRE_TIME_TOKEN",
}

interface IAuthorizationInputData {
  username: string,
  password: string,
}

enum Roles {
  ADMIN = "ADMIN",
  STUDENT = "STUDENT",
  TEACHER = "TEACHER",
}

declare namespace NToken {

  interface ICodedTokenResponse {
    refreshToken: string,
    token: string,
    tokenExpireTime: string,
  }

  interface IDecodedToken {
    g_auth: Array<string>,
    exp: number,
    iat: number,
    iss: string,
    sub: string,
    f_name: string,
    l_name: string,
    u_photo: string,
  }
}

export {
  IAuthorizationInputData,
  NToken,
  StoreKeys,
  Roles,
}