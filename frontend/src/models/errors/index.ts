interface IErrorResponseProps {
  message: string,
  stack: string,
}

interface ISystemResponseProps extends IErrorResponseProps {
  body: object,
  bodyUsed: boolean,
  headers: object,
  ok: boolean,
  redirected: boolean,
  status: number,
  statusText: string,
  type: string,
  url: string,
  json: Function,
  text: Function,
  errorCode?: number,
  clone?: Function,
}

export {
  IErrorResponseProps,
  ISystemResponseProps,
}