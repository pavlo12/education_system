import { AuthService, } from "../Auth.service";
import { HandlerOnErrorService, } from "../HandlerOnError.service";
import { PathsService, } from "../Paths.service";
import { DI, } from "../../di.config";
import { ISystemResponseProps, } from "../../models/errors";
import { injectable, } from "inversify";

@injectable()
export abstract class ApiClientBase {

	readonly authService = DI.resolve(AuthService);
	readonly path = DI.resolve(PathsService);
	readonly errorService: HandlerOnErrorService = DI.resolve(HandlerOnErrorService);

	private _doFetch(url: string, method: string, data: any, headers: any = {}) {
		const init = {
			method: method,
			body: data,
			headers: {
				...headers,
			},
		};

		if (this.authService.isAuthenticated()) {
			init.headers["Authorization"] = "Bearer " + this.authService.getStoredRawToken();
		}

		let promise: Promise<any> = fetch(url, init)
			.then((response: any): any => {
				return this.errorService.checkResponseStatus(response, method);
			})
			.catch((err: ISystemResponseProps): Promise<any> | undefined => {

				const errClone = err.clone();
				return this.errorService.checkErrorStatus(err);
			});

		return promise;
	}

	private _fetchJson(url: string, method: string, data: any = null) {
		if (data) {
			data = typeof data === "string" ? data : JSON.stringify(data);
		}

		return this._doFetch(url, method, data, {
			"Content-Type": "application/json",
		})
	}

	private _fetchFormData(url: string, method: string, data: any = null) {
		return this._doFetch(url, method, data)
	}

	postFormData(url: string, data: any) {
		return this._fetchFormData(url, "POST", data);
	}

	putJSON(url: string, data: any) {
		return this._fetchJson(url, "PUT", data);
	}

	postJSON(url: string, data: any) {
		return this._fetchJson(url, "POST", data);
	}

	deleteJSON(url: string, data?: any) {
		return this._fetchJson(url, "DELETE", data);
	}

	getJSON(url: string, data?: any) {
		return this._fetchJson(url, "GET", data);
	}

	create(data: any) {
	  return this.postJSON(this.path.toAPIFullPath(this.getRootPath()), data);
  }

  update(data: any) {
    return this.putJSON(this.path.toAPIFullPath(this.getRootPath()), data);
  }

  getOne(id: string) {
    return this.getJSON(this.path.toAPIFullPath(`${this.getRootPath()}/${id}`));
  }

  delete(id: string) {
    return this.deleteJSON(this.path.toAPIFullPath(`${this.getRootPath()}/${id}`));
  }

  getAll() {
    return this.getJSON(this.path.toAPIFullPath(this.getRootPath()));
  }

  getPage(page: number, size: number) {
    return this.getJSON(this.path.toAPIFullPath(`${this.getRootPath()}/${page}/${size}`));
  }

	protected abstract getRootPath(): string;
}
