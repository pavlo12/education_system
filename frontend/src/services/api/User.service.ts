import { ApiClientBase, } from "./ClientBase.api";
import { injectable, } from "inversify";

@injectable()
export class UserService extends ApiClientBase {

  protected getRootPath(): string {
    return "users";
  }

}