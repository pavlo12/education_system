import React from "react";
import {
	Modal,
	notification,
	message,
} from "antd";
import { PathsService, } from "./Paths.service";

import { injectable, } from "inversify";
import { DI, } from "../di.config";
import { ISystemResponseProps, } from "../models/errors";
type Notification_Message_Types = "success" | "error" | "info" | "warning" | "warn";

@injectable()
export class NotificationService {

	static duration = 6;
	readonly path = DI.resolve(PathsService);

	private showErrorModalWindow(error: ISystemResponseProps) {
		if (error.status) {
			Modal.error({
				title: `${error.status}`,
				content: error.statusText === "" ? `URL: ${error.url}` : `Status text: ${error.statusText}` + ` ` + `URL: ${error.url}`,
				okText: "OK",
			});
		} else {
			localStorage.clear();
			Modal.error({
				title: `${error.message}`,
				content: `${error.stack}`,
				okText: "OK",
			});
		}
	}

	showDefaultSuccessMessage() {
		const successMessage: string = "Операція успішно завершена";
		message.success(successMessage, 4);
	}


	showDefaultErrorMessage() {
		const errorMessage: string = "Сталася помилка";
		message.error(errorMessage, 4);
	}

	showMessageWithCustomText(message: string, code?: string | number, duration?: boolean, type?: Notification_Message_Types) {
		const TYPE: Notification_Message_Types = type || "error";
		const DURATION: boolean = duration || true;
		(() => notification[TYPE]({
			duration: DURATION ? NotificationService.duration : null,
			message: (
				<span style={{ fontSize: 14, fontStyle: "italic", }}>{code ? code : "Помилка"}</span>
			),
			description: (
				<span style={{ fontSize: 14, fontWeight: 600, }}>{message}</span>
			),
		}))();
	}

	showConfirmModalWindow(title: string, text: string, onOk: Function, okBtnText?: string, cancelBtnText?: string) {
		(() => {
			Modal.confirm({
				title: title,
				content: text,
				onOk() {
					onOk();
				},
				okText: okBtnText || "Підтвердити",
				cancelText: cancelBtnText || "Відмінити",
			});
		})();
	}

	badRequestToRefreshToken() {
		const code: string = "Неможливо надіслати запит для оновлення токена";
		const message: string = "Проблема з доступом до мережі або доступом до сервера";
		this.showMessageWithCustomText(message, code);
	}

	badAuthorizationData() {
		const code: string = "Хибна авторизація";
		const message: string = "Введено невірний пароль або користувач немає доступу! Спробуйте ще раз.";
		this.showMessageWithCustomText(message, code);
	}

	notHavePermissions() {
		const message: string = "Ви не маєте прав доступу";
		this.showMessageWithCustomText(message, "Авторизація");
	}
}