import { injectable, } from "inversify";
import { PathsService, } from "./Paths.service";
import { NotificationService, } from "./Notification.service";
import { DI, } from "../di.config";
import { Action, } from "redux-actions";
import { ISystemResponseProps, } from "../models/errors";
import { constants, } from "../components/constants";

class MethodService {
    previousMethod: string;
    private timerId: any;
    private delay: number;

    constructor(delay: number) {
        this.delay = delay;
    }

    set setCurrentMethod(method: string) {
        this.previousMethod = method;
    }

    get getPreviousMethod() {
        return this.previousMethod;
    }

    clearMethod() {
        if (this.timerId) {
            clearTimeout(this.timerId);
        }
        this.timerId = setTimeout(() => {
            this.setCurrentMethod = null;
            this.timerId = null;
        }, this.delay);
    }
}

const methodService = new MethodService(1000);


@injectable()
export class HandlerOnErrorService {

    readonly path = DI.resolve(PathsService);
    readonly notificationService = DI.resolve(NotificationService);

    // readonly apiErrorService: ErrorService = DI.resolve(ErrorService);

    private isValidMethodType(method: string): boolean {
        if (method === "POST" || method === "PUT" || method === "DELETE") {
            if (method !== methodService.getPreviousMethod) {
                methodService.setCurrentMethod = method;
                methodService.clearMethod();
                return !![];
            }
            methodService.clearMethod();
        }
        return null;
    }

    checkResponseStatus(response: ISystemResponseProps, method?: string) {
        switch (response.status) {
            case 200: {
                if (this.isValidMethodType(method)) {
                    this.notificationService.showDefaultSuccessMessage();
                }
                return response.json();
            }
            case 201: {
                if (this.isValidMethodType(method)) {
                    this.notificationService.showDefaultSuccessMessage();
                }
                return response.json();
            }
            case 204: {
                if (this.isValidMethodType(method)) {
                    this.notificationService.showDefaultSuccessMessage();
                }
                return response.text();
            }
            default: {
                return Promise.reject(response);
            }
        }
    }


    checkErrorStatus(error: ISystemResponseProps, stopSpinners?: () => Action<any>) {
        if (stopSpinners) {
            stopSpinners();
        }

        switch (error.status) {
            case 400: {
                if (error.url === this.path.toAPIFullPath("connect/token")) {
                    this.notificationService.badAuthorizationData();
                    break
                }
                return error.json()
                    .then((res: any) => {
                        if (res.code) {
                            this.notificationService.showMessageWithCustomText(res.message, "Попередження", true, "warning");
                            return Promise.reject(res);
                        } else {
                            this.notificationService.showDefaultErrorMessage();
                            return Promise.reject(error);
                        }
                    });
            }
            case 401: {
                if (error.url === this.path.toAPIFullPath("auth/login")) {
                    this.notificationService.badAuthorizationData();
                } else {
                    localStorage.clear();
                    location.assign(constants.UNAUTHORIZED_REDIRECT_PATH);
                    this.notificationService.notHavePermissions();
                }
                break;
            }
            case 403: {
                this.notificationService.notHavePermissions();
                break;
            }
            case 404: {
                // this.notificationService.showDefaultErrorMessage();
                return Promise.reject(error);
            }
            case 500: {
                /*if (error.url === this.path.toAPIFullPath("/rooms-reservation/reservations/")) {
                        return Promise.reject(error);
                    }*/
                // this.showErrorModalWindow(error);
                this.notificationService.showDefaultErrorMessage();
                this.notificationService.showMessageWithCustomText(error.url, error.status, false);
                return Promise.reject(error);
            }
            case 502: {
                const code: string = "Помилка мережі";
                const message: string = "Неможливо підключитися до сервера. Перевірте доступу до мережі або спробуйте пізніше";
                this.notificationService.showMessageWithCustomText(message, code);
                if (location.pathname !== constants.UNAUTHORIZED_REDIRECT_PATH) {
                    setTimeout(() => {
                        localStorage.clear();
                        location.assign(constants.UNAUTHORIZED_REDIRECT_PATH)
                    }, NotificationService.duration * 1000)
                }
                break;
            }
            default: {
                // if (error.message === "Failed to fetch") {
                // 	const code: string = "Помилка мережі";
                // 	const message: string = "Неможливо підключитися до сервера. Перевірте доступу до мережі або спробуйте пізніше";
                // 	this.notificationService.showMessageWithCustomText(message, code);
                // 	if (location.pathname !== constants.UNAUTHORIZED_REDIRECT_PATH) {
                // 		setTimeout(() => {
                // 			localStorage.clear();
                // 			location.assign(constants.UNAUTHORIZED_REDIRECT_PATH)
                // 		}, NotificationService.duration * 1000)
                // 	}
                // } else
                if (error.status) {
                    this.notificationService.showMessageWithCustomText(error.url, error.status, false);
                } else {
                    this.notificationService.showDefaultErrorMessage();
                    this.notificationService.showMessageWithCustomText("Невідома помилка");
                }
                break;
            }
        }
    }

    isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}