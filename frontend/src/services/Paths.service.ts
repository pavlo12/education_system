import { injectable, } from "inversify";

@injectable()
export class PathsService implements IPathsServices {
	private static host: string;

	static get API_HOST(): string {
		return this.host;
	}

	static set API_HOST(value: string) {
		this.host = value;
	}
	
	toAPIFullPath(relative: string) {
		if (relative.indexOf("http") === 0) {
			return relative;
		}

		if (relative.indexOf("data:") === 0) {
			return relative;
		}

		if (relative[0] !== "/") {
			relative = "/" + relative;
		}


		return PathsService.API_HOST + relative;
	}
}

export interface IPathsServices {
  toAPIFullPath(value: string): string | null,
}

export const IPathsServicesID = "IPathsServices";