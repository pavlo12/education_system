import { connectedRouterRedirect, } from "redux-auth-wrapper/history4/redirect";
import { Base64, } from "js-base64";
import { PathsService, } from "services/Paths.service";
import { injectable, } from "inversify";
import { DI, } from "../di.config";
import { IAuthorizationInputData, NToken, StoreKeys, } from "../models/authorization";
import { NotificationService, } from "./Notification.service";
import { constants, } from "../components/constants";


@injectable()
export class AuthService {
	private readonly notificationMessages: NotificationService = DI.resolve(NotificationService);
	private readonly paths: PathsService = DI.resolve(PathsService);

	static authCounter = 0;
	static badResponseCounter = 0;
	static startTimer: any;

	private decodedTokenParts(values: Array<string>) {
		let outputDecodedData: any = {};
		for (let counter: number = 0; counter <= 1; counter++) {
			try {
				Object.assign(outputDecodedData, JSON.parse(atob(values[counter])));
			} catch (err) {
				Object.assign(outputDecodedData, JSON.parse(Base64.decode(values[counter])));
			}
		}
		return outputDecodedData
	}

	private getDateExpireTimeToken(tokenExpireTime: string) {
		let currentDate = new Date(Date.now());
		return currentDate.setMinutes(currentDate.getMinutes() + parseFloat(tokenExpireTime));
	}

	private getDelayToSendRefreshToken(tokenExpireTime: string) {
		return parseInt(tokenExpireTime) * 60000 - 60000; // milliseconds
	}

	private refreshTokenRequest() {
		const requestParams: RequestInit = {
			method: "GET",
			body: null,
			headers: {
        "Authorization": `Bearer ${AuthService.getStoredRefreshToken()}`,
			},
		};

		return fetch(this.paths.toAPIFullPath("refresh-token"), requestParams)
	}

	getNewToken() {
		return this.refreshTokenRequest()
			.then((response: any): any => response.json())
			.then((responseJSON: any): void => this.provider(responseJSON))
			.then(() => {
				AuthService.badResponseCounter = 0;
				this.checkExpiredTokenTime();
			})
			.catch((err: any) => {
				// console.dir(err);
				AuthService.badResponseCounter += 1;
				if (AuthService.badResponseCounter >= 3) {
					AuthService.badResponseCounter = 0;
					this.notificationMessages.badRequestToRefreshToken();
					setTimeout(() => {
						location.assign(constants.UNAUTHORIZED_REDIRECT_PATH);
						this.clearAuthInfo();
					}, NotificationService.duration * 1000);
				} else {
					setTimeout(() => {
						return this.getNewToken()
					}, constants.DELAY_TO_REFRESH_TOKEN_AGAIN)
				}
			})
	}


	provider(identityResponse: NToken.ICodedTokenResponse) {
		let token: string = identityResponse.token;
		let refreshToken: string = identityResponse.refreshToken;
		let tokenExpireTime: string = identityResponse.tokenExpireTime;
		let tokenParts: string[] = token.split(".");

		let outputDecodedDataToken: NToken.IDecodedToken = this.decodedTokenParts(tokenParts);
		let dateExpireTimeToken: number = this.getDateExpireTimeToken(tokenExpireTime);
		let delayToSendRefreshToken: number = this.getDelayToSendRefreshToken(tokenExpireTime);

		this.storeIdentity(outputDecodedDataToken);
		this.storeRawToken(token);
		this.storeRefreshToken(refreshToken);
		this.storeDateExpireTimeToken(dateExpireTimeToken);
		this.storeDelayToSendRefreshToken(delayToSendRefreshToken);
	}

	/* ---------------- CHECKING ------------------ */

	checkExpiredTokenTime = () => {
		const newToken = this.getStoredRawToken();

		if (newToken !== this.getStoredCurrentToken()) {
			console.log("HAVE GOT A NEW TOKEN");

			let delayToRefreshTokenFromServer: number = this.getStoredDelayToSendRefreshToken();
			delayToRefreshTokenFromServer > constants.MAX_TIME_FOR_SET_TIMEOUT ? delayToRefreshTokenFromServer = constants.MAX_TIME_FOR_SET_TIMEOUT : null;

			clearTimeout(AuthService.startTimer);
			this.storeCurrentToken(newToken);
			AuthService.startTimer = setTimeout(() => this.getNewToken(), delayToRefreshTokenFromServer);
		} else {
			return
		}
	};

	isAuthenticated(): boolean {
		return !!this.getIdentity() && !!AuthService.getStoredRefreshToken();
	}

	isNotExpiredTimeToken(dateTokenExpireTime: number) {
		return Date.now() < dateTokenExpireTime
	}

	/* ------------ STORE TO LOCAL STORAGE -------------- */

	storeIdentity(data: NToken.IDecodedToken) {
		if (!data) {
			throw new Error("Can't save user identity object. Passed argument is empty.");
		}
		localStorage[StoreKeys.IDENTITY_STORE_KEY] = JSON.stringify(data);
	}

	storeRawToken(token: string) {
		if (!token) {
			throw new Error("Can't store empty token.");
		}
		localStorage[StoreKeys.RAW_TOKEN] = token;
	}

	storeRefreshToken(refreshToken: string) {
		if (!refreshToken) {
			localStorage[StoreKeys.RAW_REFRESH_TOKEN] = localStorage.getItem("RAW_REFRESH_TOKEN");
		} else {
			localStorage[StoreKeys.RAW_REFRESH_TOKEN] = refreshToken
		}
	}

	private storeCurrentToken(token: string) {
		if (!token) {
			throw new Error("Can't store empty current token.");
		} else {
			localStorage[StoreKeys.RAW_CURRENT_TOKEN] = token;
		}
	}

	storeDateExpireTimeToken(expireTime: number) {
		if (!expireTime) {
			throw new Error("Can't store empty expire time of token.")
		}
		localStorage[StoreKeys.DATE_EXPIRE_TIME_TOKEN] = JSON.stringify(expireTime)
	}

	storeDelayToSendRefreshToken(delayToSendRefreshToken: number) {
		if (!delayToSendRefreshToken) {
			throw new Error("Can't store empty expire time of token.")
		}
		localStorage[StoreKeys.DELAY_TO_SEND_REFRESH_TOKEN] = JSON.stringify(delayToSendRefreshToken)
	}

	/* ------------ GET FROM LOCAL STORAGE -------------- */

	getIdentity(): NToken.IDecodedToken | null {
		let raw = localStorage[StoreKeys.IDENTITY_STORE_KEY];
		if (!raw) {
			return null;
		}
		return JSON.parse(raw);
	}

	getLogin(): string {
	  return this.getIdentity().sub;
  }

  hasRole(role: string): boolean {
	  const identity = this.getIdentity();

  return identity.g_auth.indexOf(role) !== -1;
  }

	getStoredRawToken(): string {
		return localStorage[StoreKeys.RAW_TOKEN];
	}

	static getStoredRefreshToken(): string {
		return localStorage[StoreKeys.RAW_REFRESH_TOKEN];
	}

	private getStoredCurrentToken(): string {
		return localStorage[StoreKeys.RAW_CURRENT_TOKEN];
	}

	getStoredDateExpireTimeToken(): number {
		return parseInt(localStorage[StoreKeys.DATE_EXPIRE_TIME_TOKEN]);
	}

	getStoredDelayToSendRefreshToken(): number {
		return parseInt(localStorage[StoreKeys.DELAY_TO_SEND_REFRESH_TOKEN]);
	}

	clearAuthInfo() {
		localStorage.removeItem(StoreKeys.IDENTITY_STORE_KEY);
		localStorage.removeItem(StoreKeys.RAW_TOKEN);
		localStorage.removeItem(StoreKeys.RAW_REFRESH_TOKEN);
		localStorage.removeItem(StoreKeys.DATE_EXPIRE_TIME_TOKEN);
		localStorage.removeItem(StoreKeys.DELAY_TO_SEND_REFRESH_TOKEN);
		localStorage.removeItem(StoreKeys.RAW_CURRENT_TOKEN);
		AuthService.badResponseCounter = 0;
		clearTimeout(AuthService.startTimer);
	}
}

@injectable()
export class RequestGetToken {

  private path: PathsService = DI.resolve(PathsService);

	getToken(inputDataAuthorization: IAuthorizationInputData) {

		const requestParams: RequestInit = {
			method: "POST",
			body: JSON.stringify(inputDataAuthorization),
			headers: { "Content-Type": "application/json", },
		};

		return fetch(this.path.toAPIFullPath("auth/login"), requestParams)
	}
}


const userIsNotAuthenticatedConfig = {
	authenticatedSelector: () => {
		console.log("userIsNotAuthenticatedConfig");
		const authService = DI.resolve(AuthService);
		let dateExpireTimeToken = authService.getStoredDateExpireTimeToken();
		if (AuthService.authCounter > 10) {
			authService.clearAuthInfo();
			AuthService.authCounter = 0;
			return true
		} else {
			AuthService.authCounter += 1;
			if (authService.isAuthenticated()) {
				return !authService.isNotExpiredTimeToken(dateExpireTimeToken);
			} else {
				return true
			}
		}
	},
	redirectPath: "/",
	allowRedirectBack: false, // ???
	wrapperDisplayName: "userIsNotAuthenticated",
};

const userIsAuthenticatedConfig = {
	authenticatedSelector: () => {
    console.log("userIsAuthenticatedConfig");
		const authService = DI.resolve(AuthService);
		let dateExpireTimeToken = authService.getStoredDateExpireTimeToken();
		if (authService.isAuthenticated()) {
			return authService.isNotExpiredTimeToken(dateExpireTimeToken);
		} else {
			return false
		}
	},
	redirectPath: constants.UNAUTHORIZED_REDIRECT_PATH,
	redirectQueryParamName: "",
	wrapperDisplayName: "userIsAuthenticated",
};

export const userIsNotAuthenticated = connectedRouterRedirect(userIsNotAuthenticatedConfig);
export const userIsAuthenticated = connectedRouterRedirect(userIsAuthenticatedConfig);