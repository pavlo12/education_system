import { injectable, } from "inversify";


@injectable()
export class LocalizationStore implements ILocalizationService {
	getDefaultLang(): string {
		return String("en-US");
	}

	getCurrentLang(): string {
		return String(LocalizationStore.injectableLocalization);
	}

	private static injectableLocalization: any;

	static setInjectableLocalization (value: string) {
		LocalizationStore.injectableLocalization = value
	}
}

export interface ILocalizationService {
  getDefaultLang(): string;
  getCurrentLang(): string;
}

export const ILocalizationServiceID = "ILocalizationService";