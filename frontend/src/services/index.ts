﻿import { Container, } from "inversify";

import { PathsService, } from "./Paths.service";
import { LocalizationStore, } from "./LocalizationStore.service";
import { NotificationService, } from "./Notification.service";
import { AuthService, } from "./Auth.service";
import { HandlerOnErrorService, } from "./HandlerOnError.service";

export {
	PathsService,
	LocalizationStore,
  NotificationService,
  AuthService,
  HandlerOnErrorService,
};

export function registerServices(container: Container) {
	container.bind(PathsService).toSelf().inSingletonScope();
	container.bind(LocalizationStore).toSelf().inSingletonScope();
	container.bind(NotificationService).toSelf().inSingletonScope();
	container.bind(AuthService).toSelf().inSingletonScope();
	container.bind(HandlerOnErrorService).toSelf().inSingletonScope();
}