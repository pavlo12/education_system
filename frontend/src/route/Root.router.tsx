import React from "react";
import { Hello, } from "../components/Hello";
import { Icon, } from "antd";
import { Provider, } from "react-redux";
import { BrowserRouter, Route, Switch, } from "react-router-dom";
import { PublicLayout, } from "../components/layout";
import configureStore from "../store/store";
import { AuthService, userIsAuthenticated, userIsNotAuthenticated, } from "services/Auth.service";
import { constants, } from "../components/constants";
import { LoginPage, } from "../components/login";
import { DI, } from "../di.config";
import { Roles, } from "../models/authorization";


export interface IPanelRoute {
  component: any,
  path: string,
  title: string,
  inNavMenuVisible: boolean,
  icon?: JSX.Element,
  children?: IPanelRoute[],
  exact?: boolean,
  subMenu?: ISubMenuProps,
  defaultParams?: any,
}

export interface ILinkedPanelRoute extends IPanelRoute {
  fullpath: string,
  builtFullRoute: string,
  parent?: ILinkedPanelRoute,
}

export interface ISubMenuProps {
  position: number,
  icon: JSX.Element,
  title: string,
}


class RootRouter extends React.PureComponent<any, any> {

  private readonly authService: AuthService = DI.resolve(AuthService);

  private store = configureStore();


  ADMIN_PANEL_ROUTES = (): IPanelRoute[] => {
    console.log("RE_RENDER");
    return (
      [
        {
          component: Hello,
          path: "hello",
          title: "Користувачі",
          icon: <Icon type="solution"/>,
          inNavMenuVisible: this.authService.hasRole(Roles.ADMIN),
        },
        {
          component: Hello,
          path: "hello1",
          title: "Дисципліни",
          icon: <Icon type="solution"/>,
          inNavMenuVisible: this.authService.hasRole(Roles.ADMIN),
        },
        {
          component: Hello,
          path: "hello2",
          title: "Групи",
          icon: <Icon type="solution"/>,
          inNavMenuVisible: this.authService.hasRole(Roles.ADMIN),
        }
      ]);
  };

  render() {
    return (<Provider store={this.store}>
        <BrowserRouter>
          <Switch>
            <Route path={constants.UNAUTHORIZED_REDIRECT_PATH} component={
              userIsNotAuthenticated((props: any) => <LoginPage {...props}/>)}/>
            <Route path="/" key="root" component={
              userIsAuthenticated((props: any): JSX.Element => (
                  <div style={{ height: "100%", }}>
                    <PublicLayout
                      routes={this.ADMIN_PANEL_ROUTES()}
                    />
                  </div>
                )
              )
            }/>
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default RootRouter;