import { Container, } from "inversify";

import { LocalizationStore, PathsService, registerServices, } from "./services";
import { IPathsServices, IPathsServicesID, } from "./services/Paths.service";
import { ILocalizationService, ILocalizationServiceID, } from "./services/LocalizationStore.service";


export const DI = new Container();

export class DiConfig {
  static init() {
    registerServices(DI);
    LocalizationStore.setInjectableLocalization("uk-UA");
    DI.bind<IPathsServices>(IPathsServicesID).to(PathsService);
    DI.bind<ILocalizationService>(ILocalizationServiceID).to(LocalizationStore);
  }
}