import React from "react";
import ReactDOM from "react-dom";
import { AppContainer, } from "react-hot-loader";
import moment from "moment";
import "moment/locale/uk";
import { DiConfig, } from "./di.config";
import { PathsService, } from "./services";
import RootRouter from "./route/Root.router";

const mountPoint = document.getElementById("root");


moment.locale("uk");

const render = (Comp: any) =>
    ReactDOM.render(
        <AppContainer>
            <Comp/>
        </AppContainer>,
        mountPoint,
    );

export const loadConfig = () => {
    return fetch("/app.config.json")
        .then(response => response.json())
};


loadConfig()
    .then(config => PathsService.API_HOST = config.apiHostName)
    .then(() => DiConfig.init())
    .then(() => render(RootRouter));



