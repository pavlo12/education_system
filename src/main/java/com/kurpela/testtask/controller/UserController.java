package com.kurpela.testtask.controller;

import com.kurpela.testtask.entity.User;
import com.kurpela.testtask.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/users")
public class UserController extends AbstractRestController<User, String> {

  protected UserController(UserService service) {
    super(service);
  }
}
