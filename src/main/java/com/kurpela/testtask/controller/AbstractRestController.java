package com.kurpela.testtask.controller;

import com.kurpela.testtask.entity.Identifiable;
import com.kurpela.testtask.service.Service;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


public abstract class AbstractRestController<E extends Identifiable<K>, K extends Serializable>
        implements Controller<E, K> {
  protected Service<E, K> service;

  protected AbstractRestController(Service<E, K> service) {
    this.service = service;
  }

  @RequestMapping(
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE,
      path = "/{id}"
  )
  @ResponseBody
  public HttpEntity<E> findById(@PathVariable K id) {

    E entity = service.find(id);

    if (entity == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return ResponseEntity.ok(entity);
  }

  @RequestMapping(path = "/{page}/{limit}")
  public HttpEntity<Page<E>> getPage(
      @PathVariable Integer page,
      @PathVariable Integer limit
  ) {
    final PageRequest pageable = new PageRequest(page, limit);
    final Page<E> entityPage = service.findAll(pageable);
    if (entityPage.getTotalPages() - 1 < page) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok(entityPage);
  }

  @RequestMapping
  public HttpEntity<List<E>> getAll() {
    return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.POST)
  @SuppressWarnings("unchecked")
  public HttpEntity<E> create(@RequestBody E product) {
    product = service.create(product);

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(product);
  }

//  @RequestMapping(
//      method = RequestMethod.PUT
////      produces = MediaType.APPLICATION_JSON_VALUE,
////          path = "/{id}"
//  )
//  public HttpEntity<Resource<E>> update(
//           K id, E product) {
////    @PathVariable final K id, @RequestBody final E product) {
//    return doUpdate(id, product);
//  }


  @RequestMapping(
      method = RequestMethod.PUT
//      produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ResponseBody public HttpEntity<E> update(@RequestBody E entity) {

    final E updatedEntity= service.update(entity);

    return ResponseEntity.ok(updatedEntity);
  }

//  public HttpEntity<Resource<E>> doUpdate(final K id, final E product) {
//    if (!product.getId().equals(id)) {
//      throw new EntityIdMismatchWebException();
//    }
//
//    final E updatedProduct = service.update(product);
//
//    final Resource<E> resource = assembler.toResource(updatedProduct);
//    return new ResponseEntity<>(resource, HttpStatus.OK);
//  }

  @RequestMapping(
      method = RequestMethod.DELETE,
      produces = MediaType.APPLICATION_JSON_VALUE,
      path = "{id}"
  )
  @ResponseBody public HttpEntity<?> remove(@PathVariable K id) {
    service.delete(id);
    return ResponseEntity.noContent().build();
  }

}
