package com.kurpela.testtask.controller;

import com.kurpela.testtask.entity.Identifiable;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface Controller<E extends Identifiable<K>, K extends Serializable> {
    HttpEntity<E> findById(K id);

    HttpEntity<Page<E>> getPage(
        @PathVariable Integer page,
        @PathVariable Integer limit
    );

   HttpEntity<List<E>> getAll();

   HttpEntity<E> create(@RequestBody E product);

    HttpEntity<E> update(E product);

    HttpEntity<?> remove(K id);
}
