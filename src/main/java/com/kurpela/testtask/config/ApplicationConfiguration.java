package com.kurpela.testtask.config;

import com.kurpela.testtask.service.DefaultDataSeedPopulationService;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

@Configuration
//@ComponentScan("com.resolff.vmed")
public class ApplicationConfiguration implements ApplicationListener<ContextRefreshedEvent> {
//  @Setter(onMethod = @__(@Autowired))
//  private AuthorizationService authorizationService;

//  @Setter(onMethod = @__(@Autowired))
//  private DBAppenderService dbAppenderService;

  @Setter(onMethod = @__(@Autowired))
  private DefaultDataSeedPopulationService defaultDataSeedPopulationService;


  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
//    dbAppenderService.initDBTables();
    defaultDataSeedPopulationService.populate();
//    authorizationService.switchToInitializedState();
  }

//  @Bean
//  public MultipartResolver multipartResolver() {
//    return new StandardServletMultipartResolver() {
//      @Override
//      public boolean isMultipart(HttpServletRequest request) {
//        String method = request.getMethod().toLowerCase();
//        if (!Arrays.asList("put", "post").contains(method)) {
//          return false;
//        }
//        String contentType = request.getContentType();
//        return (contentType != null && contentType.toLowerCase().startsWith("multipart/"));
//      }
//    };
//  }

}

