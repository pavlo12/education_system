package com.kurpela.testtask.security.web.controller;

import com.kurpela.testtask.entity.User;
import com.kurpela.testtask.security.auth.jwt.extractors.TokenExtractor;
import com.kurpela.testtask.security.auth.jwt.verifiers.TokenVerifier;
import com.kurpela.testtask.security.config.JwtSettings;
import com.kurpela.testtask.security.config.WebSecurityConfig;
import com.kurpela.testtask.security.domain.AuthorizationContext;
import com.kurpela.testtask.security.domain.token.RawAccessJwtToken;
import com.kurpela.testtask.security.domain.token.RefreshToken;
import com.kurpela.testtask.security.exception.InvalidJwtTokenException;
import com.kurpela.testtask.security.service.JwtTokenFactory;
import com.kurpela.testtask.service.UserService;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RefreshTokenEndpoint {
  private final JwtTokenFactory tokenFactory;
  private final JwtSettings jwtSettings;
  private final TokenVerifier tokenVerifier;
  private final UserService userService;

  @Qualifier("jwtHeaderTokenExtractor")
  private final TokenExtractor tokenExtractor;


  @RequestMapping(value = "/auth/token", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
  public @ResponseBody
  Map<String, String> refreshToken(HttpServletRequest request) throws InvalidJwtTokenException {
    String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));

    RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
    RefreshToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey())
        .orElseThrow(InvalidJwtTokenException::new);

    String jti = refreshToken.getJti();
    if (!tokenVerifier.verify(jti)) {
      throw new InvalidJwtTokenException();
    }

    String subject = refreshToken.getSubject();

    User user = userService.find(subject);

    if (user == null) {
      throw new UsernameNotFoundException("User not found: " + subject);
    }


    AuthorizationContext authorizationContext = AuthorizationContext.from(user);

    Map<String, String> tokenMap = new HashMap<>();
    tokenMap.put("token", tokenFactory.createAccessJwtToken(authorizationContext).getToken());
    tokenMap.put("tokenExpireTime", jwtSettings.getTokenExpirationTime().toString());

    return tokenMap;
  }
}
