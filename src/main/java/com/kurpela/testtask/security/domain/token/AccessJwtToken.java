package com.kurpela.testtask.security.domain.token;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public final class AccessJwtToken implements JwtToken {
  private final String token;

  @JsonIgnore
  private final Claims claims;
}
