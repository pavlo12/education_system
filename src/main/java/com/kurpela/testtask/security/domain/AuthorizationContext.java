package com.kurpela.testtask.security.domain;

import com.kurpela.testtask.entity.User;
import io.jsonwebtoken.Claims;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Getter
public class AuthorizationContext {

    private final String username;
    private final String firstName;
    private final String lastName;
    private final String photoPath;

    private Set<GrantedAuthority> authoritySet;

    private AuthorizationContext(String username,
                                 String firstName,
                                 String lastName,
                                 String photoPath,
                                 Set<GrantedAuthority> authorities) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photoPath = photoPath;
        this.authoritySet = authorities;
    }

    public static AuthorizationContext from(final User user) {

        Set<GrantedAuthority> authorities = user.getRoles().stream()
            .map(SimpleGrantedAuthority::new).collect(Collectors.toSet());

        return new AuthorizationContext(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoto(),
                authorities);
    }



    @SuppressWarnings("unchecked")
    public static AuthorizationContext from(final Claims claims) {
        String subject = claims.getSubject();
        List<String> scopes = claims.get(KnownClaimKeys.AUTHORITIES, List.class);
        String fname = claims.get(KnownClaimKeys.FIRST_NAME, String.class);
        String lname = claims.get(KnownClaimKeys.LAST_NAME, String.class);
        String photo = claims.get(KnownClaimKeys.USER_PHOTO, String.class);

      Set<GrantedAuthority> authorities = scopes.stream()
          .map(SimpleGrantedAuthority::new)
          .collect(Collectors.toSet());

        return new AuthorizationContext(subject,
                fname,
                lname,
                photo,
                authorities);
    }


    public void export(Claims claims) {
        claims.setSubject(getUsername());
        List<String> authorityNames = authoritySet.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        claims.put(KnownClaimKeys.AUTHORITIES, authorityNames);
        claims.put(KnownClaimKeys.FIRST_NAME, getFirstName());
        claims.put(KnownClaimKeys.LAST_NAME, getLastName());
        claims.put(KnownClaimKeys.USER_PHOTO, getPhotoPath());
    }

}
