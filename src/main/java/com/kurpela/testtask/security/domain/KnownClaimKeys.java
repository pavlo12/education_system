package com.kurpela.testtask.security.domain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class KnownClaimKeys {
  public static final String AUTHORITIES = "g_auth";
  public static final String FIRST_NAME = "f_name";
  public static final String LAST_NAME = "l_name";
  public static final String USER_PHOTO = "u_photo";
}
