package com.kurpela.testtask.security.domain.token;

public interface JwtToken {
  String getToken();
}
