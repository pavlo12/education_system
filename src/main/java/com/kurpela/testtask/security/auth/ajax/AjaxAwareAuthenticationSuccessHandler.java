package com.kurpela.testtask.security.auth.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kurpela.testtask.security.config.JwtSettings;
import com.kurpela.testtask.security.domain.AuthorizationContext;
import com.kurpela.testtask.security.domain.token.JwtToken;
import com.kurpela.testtask.security.service.JwtTokenFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class AjaxAwareAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
  private final ObjectMapper mapper;
  private final JwtTokenFactory tokenFactory;
  private final JwtSettings jwtSettings;

  @Autowired
  public AjaxAwareAuthenticationSuccessHandler(
      final ObjectMapper mapper, final JwtTokenFactory tokenFactory, JwtSettings jwtSettings) {
    this.mapper = mapper;
    this.tokenFactory = tokenFactory;
    this.jwtSettings = jwtSettings;
  }

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                      Authentication authentication) throws IOException {
    AuthorizationContext authorizationContext
        = (AuthorizationContext) authentication.getPrincipal();

    JwtToken accessToken = tokenFactory.createAccessJwtToken(authorizationContext);
    JwtToken refreshToken = tokenFactory.createRefreshToken(authorizationContext);


    Map<String, String> tokenMap = new HashMap<>();
    tokenMap.put("token", accessToken.getToken());
    tokenMap.put("refreshToken", refreshToken.getToken());
    tokenMap.put("tokenExpireTime", jwtSettings.getTokenExpirationTime().toString());

    response.setStatus(HttpStatus.OK.value());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    mapper.writeValue(response.getWriter(), tokenMap);

    clearAuthenticationAttributes(request);
  }

  protected final void clearAuthenticationAttributes(HttpServletRequest request) {
    HttpSession session = request.getSession(false);

    if (session == null) {
      return;
    }

    session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
  }
}
