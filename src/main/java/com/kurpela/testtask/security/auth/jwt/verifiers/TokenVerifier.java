package com.kurpela.testtask.security.auth.jwt.verifiers;

public interface TokenVerifier {
  boolean verify(String jti);
}
