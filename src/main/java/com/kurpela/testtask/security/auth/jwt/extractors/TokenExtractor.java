package com.kurpela.testtask.security.auth.jwt.extractors;

public interface TokenExtractor {
  String extract(String payload);
}
