package com.kurpela.testtask.security.auth.jwt;

import com.kurpela.testtask.security.auth.JwtAuthenticationToken;
import com.kurpela.testtask.security.auth.jwt.extractors.TokenExtractor;
import com.kurpela.testtask.security.config.WebSecurityConfig;
import com.kurpela.testtask.security.domain.token.RawAccessJwtToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class JwtTokenAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {
  private final AuthenticationFailureHandler failureHandler;
  private final TokenExtractor tokenExtractor;

  public JwtTokenAuthenticationProcessingFilter(
      AuthenticationFailureHandler failureHandler,
      TokenExtractor tokenExtractor, RequestMatcher matcher) {
    super(matcher);
    this.failureHandler = failureHandler;
    this.tokenExtractor = tokenExtractor;
  }

  @Override
  public Authentication attemptAuthentication(
      HttpServletRequest request, HttpServletResponse response)
      throws AuthenticationException {

    String tokenPayload = request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM);

    if(tokenPayload == null) {
      tokenPayload = request.getParameter(WebSecurityConfig.JWT_TOKEN_QUERYSTRING_PARAM);
    }

    if (tokenPayload == null) {
      List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
      grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ANONYMOUS"));

      return new AnonymousAuthenticationToken(
          "anonymousUser", "anonymousUser", grantedAuthorities);
    }

    RawAccessJwtToken token = new RawAccessJwtToken(tokenExtractor.extract(tokenPayload));
    Authentication authentication = getAuthenticationManager().authenticate(new JwtAuthenticationToken(token));
    return authentication;
  }

  @Override
  protected void successfulAuthentication(
      HttpServletRequest request, HttpServletResponse response, FilterChain chain,
      Authentication authResult) throws IOException, ServletException {
    SecurityContext context = SecurityContextHolder.createEmptyContext();
    context.setAuthentication(authResult);
    SecurityContextHolder.setContext(context);
    chain.doFilter(request, response);
  }

  @Override
  protected void unsuccessfulAuthentication(
      HttpServletRequest request, HttpServletResponse response,
      AuthenticationException failed) throws IOException, ServletException {
    SecurityContextHolder.clearContext();
    failureHandler.onAuthenticationFailure(request, response, failed);
  }
}
