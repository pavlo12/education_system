package com.kurpela.testtask.security.auth.jwt;

import com.kurpela.testtask.security.auth.JwtAuthenticationToken;
import com.kurpela.testtask.security.config.JwtSettings;
import com.kurpela.testtask.security.domain.AuthorizationContext;
import com.kurpela.testtask.security.domain.token.RawAccessJwtToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
@SuppressWarnings("unchecked")
public class JwtAuthenticationProvider implements AuthenticationProvider {
  private final JwtSettings jwtSettings;

  @Autowired
  public JwtAuthenticationProvider(JwtSettings jwtSettings) {
    this.jwtSettings = jwtSettings;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    RawAccessJwtToken rawAccessToken = (RawAccessJwtToken) authentication.getCredentials();

    Jws<Claims> jwsClaims = rawAccessToken.parseClaims(jwtSettings.getTokenSigningKey());
    Claims claims = jwsClaims.getBody();

    AuthorizationContext context = AuthorizationContext.from(claims);

    return new JwtAuthenticationToken(context, context.getAuthoritySet());
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
  }
}
