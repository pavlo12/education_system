package com.kurpela.testtask.security.auth;

import com.kurpela.testtask.security.domain.AuthorizationContext;
import com.kurpela.testtask.security.domain.token.RawAccessJwtToken;
import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class JwtAuthenticationToken extends AbstractAuthenticationToken {
  private static final long serialVersionUID = 2877954820905567501L;

  private RawAccessJwtToken rawAccessToken;
  private AuthorizationContext authorizationContext;

  public JwtAuthenticationToken(RawAccessJwtToken unsafeToken) {
    super(null);
    this.rawAccessToken = unsafeToken;
    this.setAuthenticated(false);
  }

  public JwtAuthenticationToken(AuthorizationContext authorizationContext,
                                Collection<? extends GrantedAuthority> authorities) {
    super(authorities);
    this.eraseCredentials();
    this.authorizationContext = authorizationContext;
    super.setAuthenticated(true);
  }

  @Override
  public void setAuthenticated(boolean authenticated) {
    if (authenticated) {
      throw new IllegalArgumentException(
          "Cannot set this token to trusted "
              + "- use constructor which takes a GrantedAuthority list instead");
    }
    super.setAuthenticated(false);
  }

  @Override
  public Object getCredentials() {
    return rawAccessToken;
  }

  @Override
  public Object getPrincipal() {
    return this.authorizationContext;
  }

  @Override
  public void eraseCredentials() {
    super.eraseCredentials();
    this.rawAccessToken = null;
  }
}
