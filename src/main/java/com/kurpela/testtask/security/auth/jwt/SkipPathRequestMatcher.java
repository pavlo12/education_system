package com.kurpela.testtask.security.auth.jwt;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class SkipPathRequestMatcher implements RequestMatcher {
  private OrRequestMatcher matchers;

  public SkipPathRequestMatcher(List<RequestMatcher> matchers) {
    this.matchers = new OrRequestMatcher(matchers);
  }

  @Override
  public boolean matches(HttpServletRequest request) {
    return !matchers.matches(request);
  }
}
