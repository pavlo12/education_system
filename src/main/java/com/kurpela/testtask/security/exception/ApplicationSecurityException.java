package com.kurpela.testtask.security.exception;


import com.kurpela.testtask.exception.ApplicationException;

public class ApplicationSecurityException extends ApplicationException {
  public ApplicationSecurityException(Enum<?> code, String message) {
    super(code, message);
  }

  public ApplicationSecurityException(Enum<?> code, String message, Throwable cause) {
    super(code, message, cause);
  }
}
