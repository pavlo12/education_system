package com.kurpela.testtask.security.exception;

public class NotAuthenticatedAccessException extends ApplicationSecurityException {

  private static final String MESSAGE = "Не авторизовано!";

  public NotAuthenticatedAccessException() {
    super(NotAuthorizedAccessErrorCode.NOT_AUTHENTICATED, MESSAGE);
  }

  public NotAuthenticatedAccessException(Throwable cause) {
    super(NotAuthorizedAccessErrorCode.NOT_AUTHENTICATED, MESSAGE, cause);
  }

  public enum NotAuthorizedAccessErrorCode {
    NOT_AUTHENTICATED
  }
}
