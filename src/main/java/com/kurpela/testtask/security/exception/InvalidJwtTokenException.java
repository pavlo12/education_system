package com.kurpela.testtask.security.exception;

public class InvalidJwtTokenException extends ApplicationSecurityException {
  private static final long serialVersionUID = -294671188037098603L;

  public InvalidJwtTokenException() {
    super(null, null);
  }

  public InvalidJwtTokenException(Throwable cause) {
    super(null, null, cause);
  }
}
