package com.kurpela.testtask.security.exception;

public class NotAuthorizedAccessException extends ApplicationSecurityException {

  private static final String MESSAGE = "Не має дозволу для виконання цієї дії";

  public NotAuthorizedAccessException() {
    super(NotAuthorizedAccessErrorCode.NOT_ACCESS, MESSAGE);
  }

  public NotAuthorizedAccessException(Throwable cause) {
    super(NotAuthorizedAccessErrorCode.NOT_ACCESS, MESSAGE, cause);
  }

  public enum NotAuthorizedAccessErrorCode {
    NOT_ACCESS
  }
}
