package com.kurpela.testtask.security.exception;

public class NotAccessException extends ApplicationSecurityException {
  public NotAccessException() {
    super(null, null);
  }

  public NotAccessException(Throwable cause) {
    super(null, null, cause);
  }
}
