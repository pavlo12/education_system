package com.kurpela.testtask.security.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.AbstractEnvironment;

@Getter
@Setter
@Configuration
public class JwtSettings {
  public static final String CONFIG_PATH;
  static {
    CONFIG_PATH = "application.properties";
  }
  private String strTokenExpirationTime;
  private String tokenIssuer;
  private String tokenSigningKey;
  private String strRefreshTokenExpTime;

  public JwtSettings() {
    read();
  }

  public Integer getRefreshTokenExpTime() {
    return Integer.valueOf(strRefreshTokenExpTime);
  }

  public Integer getTokenExpirationTime() {
    return Integer.valueOf(strTokenExpirationTime);
  }

  private void read() {
    ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    final Properties prop = new Properties();
    try (final InputStream stream =
             classloader.getResourceAsStream(CONFIG_PATH)) {
      prop.load(stream);
      strTokenExpirationTime = prop.getProperty("tokenExpirationTime");
      tokenIssuer = prop.getProperty("tokenIssuer");
      tokenSigningKey = prop.getProperty("tokenSigningKey");
      strRefreshTokenExpTime = prop.getProperty("refreshTokenExpTime");
    } catch (IOException ignored) {

    }
  }
}
