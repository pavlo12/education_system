package com.kurpela.testtask.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kurpela.testtask.security.auth.ajax.AjaxAuthenticationProvider;
import com.kurpela.testtask.security.auth.ajax.AjaxLoginProcessingFilter;
import com.kurpela.testtask.security.auth.jwt.JwtAuthenticationProvider;
import com.kurpela.testtask.security.auth.jwt.JwtTokenAuthenticationProcessingFilter;
import com.kurpela.testtask.security.auth.jwt.SkipPathRequestMatcher;
import com.kurpela.testtask.security.auth.jwt.extractors.TokenExtractor;
import com.kurpela.testtask.security.filter.CustomCorsFilter;
import com.kurpela.testtask.security.web.controller.RestAuthenticationEntryPoint;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.transaction.annotation.Transactional;


@Configuration
@AllArgsConstructor(onConstructor = @__(@Autowired))
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  public static final String JWT_TOKEN_QUERYSTRING_PARAM = "accessToken";
  public static final String JWT_TOKEN_HEADER_PARAM = "Authorization";
  public static final String FORM_BASED_LOGIN_ENTRY_POINT = "/auth/login";
  public static final String TOKEN_REFRESH_ENTRY_POINT = "/auth/token";

  private final RestAuthenticationEntryPoint authenticationEntryPoint;
  private final AjaxAuthenticationProvider ajaxAuthenticationProvider;
  private final JwtAuthenticationProvider jwtAuthenticationProvider;
  private final AuthenticationSuccessHandler successHandler;
  private final ObjectMapper objectMapper;
  private final AuthenticationFailureHandler failureHandler;
  private final TokenExtractor tokenExtractor;


  private AjaxLoginProcessingFilter buildAjaxLoginProcessingFilter(
      AuthenticationManager authenticationManager) {
    AjaxLoginProcessingFilter filter
        = new AjaxLoginProcessingFilter(
        FORM_BASED_LOGIN_ENTRY_POINT, successHandler, failureHandler, objectMapper);
    filter.setAuthenticationManager(authenticationManager);
    return filter;
  }

  private JwtTokenAuthenticationProcessingFilter buildJwtTokenAuthenticationProcessingFilter(
      AuthenticationManager authenticationManager) {

    List<RequestMatcher> matchers = new ArrayList<>();
    matchers.add(new AntPathRequestMatcher(TOKEN_REFRESH_ENTRY_POINT));
    JwtTokenAuthenticationProcessingFilter filter
        = new JwtTokenAuthenticationProcessingFilter(
        failureHandler, tokenExtractor, new SkipPathRequestMatcher(matchers));
    filter.setAuthenticationManager(authenticationManager);
    return filter;
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(ajaxAuthenticationProvider);
    auth.authenticationProvider(jwtAuthenticationProvider);
  }

  @Override
  @Transactional
  protected void configure(HttpSecurity http) throws Exception {

    AuthenticationManager authenticationManager = authenticationManager();

    http.csrf().disable()
        .exceptionHandling()
        .authenticationEntryPoint(this.authenticationEntryPoint)

        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        .and()
        .authorizeRequests()
        .antMatchers("/users").hasAuthority("ADMIN")
        .antMatchers("/h2-console/**").anonymous()
        .antMatchers(FORM_BASED_LOGIN_ENTRY_POINT).permitAll() // Login end-point
        .antMatchers(TOKEN_REFRESH_ENTRY_POINT).permitAll() // Token refresh end-point
        .and()
        .addFilterBefore(new CustomCorsFilter(), UsernamePasswordAuthenticationFilter.class)
        .addFilterBefore(buildAjaxLoginProcessingFilter(authenticationManager),
            UsernamePasswordAuthenticationFilter.class)
        .addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(authenticationManager),
            UsernamePasswordAuthenticationFilter.class);
  }
}
