package com.kurpela.testtask.security.service;


import com.kurpela.testtask.security.domain.AuthorizationContext;
import com.kurpela.testtask.security.domain.token.AccessJwtToken;
import com.kurpela.testtask.security.domain.token.JwtToken;

public interface JwtTokenFactory {
  AccessJwtToken createAccessJwtToken(AuthorizationContext authorizationContext);

  JwtToken createRefreshToken(AuthorizationContext authorizationContext);
}
