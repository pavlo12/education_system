package com.kurpela.testtask.security.service.impl;


import com.kurpela.testtask.security.config.JwtSettings;
import com.kurpela.testtask.security.domain.AuthorizationContext;
import com.kurpela.testtask.security.domain.KnownClaimKeys;
import com.kurpela.testtask.security.domain.Scopes;
import com.kurpela.testtask.security.domain.token.AccessJwtToken;
import com.kurpela.testtask.security.domain.token.JwtToken;
import com.kurpela.testtask.security.service.JwtTokenFactory;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenFactoryImpl implements JwtTokenFactory {

  private final JwtSettings settings;

  @Autowired
  public JwtTokenFactoryImpl(JwtSettings settings) {
    this.settings = settings;
  }

  @Override
  public AccessJwtToken createAccessJwtToken(AuthorizationContext authorizationContext) {
    if (StringUtils.isBlank(authorizationContext.getUsername())) {
      throw new IllegalArgumentException("Cannot create JWT Token without username");
    }

    if (authorizationContext.getAuthoritySet() == null
        || authorizationContext.getAuthoritySet().isEmpty()) {
      throw new IllegalArgumentException("User doesn't have any privileges");
    }

    Claims claims = Jwts.claims();
    authorizationContext.export(claims);

    return buildJwtToken(settings.getTokenExpirationTime(), claims);
  }

  @Override
  public JwtToken createRefreshToken(AuthorizationContext authorizationContext) {
    if (StringUtils.isBlank(authorizationContext.getUsername())) {
      throw new IllegalArgumentException("Cannot create JWT Token without username");
    }

    Claims claims = Jwts.claims();
//    claims.put(KnownClaimKeys.IS_SUPER_ADMIN, authorizationContext.isSuperAdmin());
//    claims.put(KnownClaimKeys.ORGANIZATION_ID, authorizationContext.getOrganizationId());
    claims.put(KnownClaimKeys.AUTHORITIES, Collections.singleton(Scopes.REFRESH_TOKEN.authority()));
    claims.setSubject(authorizationContext.getUsername());

    return buildJwtToken(settings.getRefreshTokenExpTime(), claims, UUID.randomUUID());
  }

  private AccessJwtToken buildJwtToken(int expirationTime, Claims claims) {
    return buildJwtToken(expirationTime, claims, null);
  }

  private AccessJwtToken buildJwtToken(int expirationTime, Claims claims, UUID tokenId) {
    LocalDateTime currentTime = LocalDateTime.now();

    Date expiration = Date.from(
        currentTime
            .plusMinutes(expirationTime)
            .atZone(ZoneId.systemDefault()).toInstant()
    );
    Date issuedAt = Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant());

    JwtBuilder jwtBuilder = Jwts.builder()
        .setClaims(claims)
        .setIssuer(settings.getTokenIssuer())
        .setIssuedAt(issuedAt)
        .setExpiration(expiration);

    if (tokenId != null) {
      jwtBuilder.setId(tokenId.toString());
    }

    String token = jwtBuilder
        .signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
        .compact();

    return new AccessJwtToken(token, claims);
  }
}
