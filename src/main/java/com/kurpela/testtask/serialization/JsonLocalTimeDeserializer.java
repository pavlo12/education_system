package com.kurpela.testtask.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class JsonLocalTimeDeserializer extends JsonDeserializer<LocalTime> {

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

  @Override
  public LocalTime deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException {
    ObjectCodec oc = jp.getCodec();
    ObjectNode objectNode = oc.readTree(jp);

    NumericNode houre = (NumericNode) objectNode.get("hour");
    NumericNode minute = (NumericNode) objectNode.get("minute");
    NumericNode seconde = (NumericNode) objectNode.get("second");
    NumericNode nano = (NumericNode) objectNode.get("nano");

//    TextNode node = oc.readTree(jp);
//    String dateString = node.textValue();
    return LocalTime.of(
        houre.intValue(),
        minute.intValue(),
        seconde.intValue(),
        nano.intValue()
    );
  }
}
