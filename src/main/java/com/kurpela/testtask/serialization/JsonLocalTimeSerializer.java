package com.kurpela.testtask.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class JsonLocalTimeSerializer extends JsonSerializer<LocalTime> {

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

  @Override
  public void serialize(LocalTime time, JsonGenerator generator, SerializerProvider arg2)
      throws IOException {

    String stringTime = time.format(formatter);

//    time.getLong(ChronoField.MILLI_OF_DAY);

//    ZonedDateTime zdt = time.atZone(ZoneId.systemDefault());
    generator.writeString(stringTime);
  }
}
