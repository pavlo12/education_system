package com.kurpela.testtask.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class JsonLocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {

  @Override
  public void serialize(LocalDateTime date, JsonGenerator generator, SerializerProvider arg2)
      throws IOException {

    ZonedDateTime zdt = date.atZone(ZoneId.systemDefault());
    generator.writeNumber(zdt.toInstant().toEpochMilli());
  }
}
