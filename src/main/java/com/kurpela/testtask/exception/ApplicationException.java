package com.kurpela.testtask.exception;

import lombok.Getter;

@Getter
public class ApplicationException extends RuntimeException {
  private Enum<?> code;

  private Object details;

  public ApplicationException(Enum<?> code, String message, Object details) {
    this(code, message, null);
    this.details = details;
  }

  public ApplicationException(Enum<?> code, String message) {
    this(code, message, null);
  }

  public ApplicationException(Enum<?> code, String message, Throwable cause) {
    super(message, cause);
    this.code = code;
  }
}
