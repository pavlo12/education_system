package com.kurpela.testtask.exception;

public class DataAccessException extends ApplicationException {
  public DataAccessException(Enum<?> code, String message) {
    super(code, message);
  }

  public DataAccessException(Enum<?> code, String message, Throwable cause) {
    super(code, message, cause);
  }

  public enum DataAccessErrorCode {
    ENTITY_NOT_PERSISTED,
    ORGANIZATION_OWNER_ID_MISMATCH,
    ORGANIZATION_ID_MISMATCH,
    WRONG_PERMISSION_TYPE,
    BLANK_USER_ID,
    BLANK_PERMISSION_ID,
    IMMUTABLE_PERMISSION
  }
}
