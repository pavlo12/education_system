package com.kurpela.testtask.exception;

public class EntityNotPersistedException extends DataAccessException {
  public EntityNotPersistedException() {
    this(null);
  }

  public EntityNotPersistedException(Throwable cause) {
    super(DataAccessErrorCode.ENTITY_NOT_PERSISTED, "Не вдається оновити об'єкт, який не зберігався.", cause);
  }
}
