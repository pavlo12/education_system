package com.kurpela.testtask.convert;

import java.sql.Date;
import java.time.LocalDate;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {

  @Override
  public Date convertToDatabaseColumn(LocalDate localDateTime) {
    return localDateTime != null ? Date.valueOf(localDateTime) : null;
  }

  @Override
  public LocalDate convertToEntityAttribute(Date timestamp) {
    return timestamp != null ? timestamp.toLocalDate() : null;
  }
}