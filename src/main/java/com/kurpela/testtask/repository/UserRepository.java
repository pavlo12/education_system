package com.kurpela.testtask.repository;

import com.kurpela.testtask.entity.User;

public interface UserRepository extends CrudRepository<User, String> {
}
