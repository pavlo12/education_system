package com.kurpela.testtask.repository;

import com.kurpela.testtask.entity.Identifiable;
import com.kurpela.testtask.entity.OrderBy;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface Repository<E extends Identifiable<K>, K extends Serializable> {

  E findOne(K id);

  boolean exists(K id);

  List<E> findAll(OrderBy... orderByInfo);

  List<E> findAll(Iterable<K> ids, OrderBy... orderByInfo);

  long count();

  List<E> findAll(Sort sort);

  Page<E> findAll(Pageable pageable, OrderBy... orderByInfo);

}
