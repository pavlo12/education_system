package com.kurpela.testtask.repository.impl;

import com.kurpela.testtask.entity.Identifiable;
import com.kurpela.testtask.repository.CrudRepository;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractCrudRepositoryImpl
    <E extends Identifiable<K>, K extends Serializable>

    extends AbstractReadRepositoryImpl<E, K>
    implements CrudRepository<E, K> {

  public AbstractCrudRepositoryImpl(
      EntityManager em, Class<E> entityClass) {
    super(em, entityClass);
  }

  @Override
  @Transactional
  public <S extends E> S create(S entity) {
    entity.setVersion(null);
    beforeCreate(entity);
    em.persist(entity);
    return entity;
  }

  @Override
  @Transactional
  public <S extends E> List<S> create(Iterable<S> entities) {
    return StreamSupport.stream(entities.spliterator(), false)
        .map(this::create)
        .collect(Collectors.toList());
  }

  @Override
  @Transactional
  public <S extends E> S update(S entity) {
    ensureEntityIsNotNew(entity);
    E oldValue = findOne(entity.getId());
    beforeUpdate(entity, oldValue);
    return em.merge(entity);
  }

  @Override
  @Transactional
  public <S extends E> List<S> update(Iterable<S> entities) {
    return StreamSupport.stream(entities.spliterator(), false)
        .map(this::update)
        .collect(Collectors.toList());
  }

  @Transactional
  @Override
  public void delete(K id) {
    E toRemove = findOne(id);

    if (toRemove == null) {
      return;
    }

    delete(toRemove);

  }

  @Override
  public void delete(E entity) {

    final CriteriaBuilder builder = em.getCriteriaBuilder();
    final CriteriaDelete<E> removeCmd = builder.createCriteriaDelete(entityClass);
    final Root<E> root = removeCmd.from(entityClass);
    removeCmd.where(builder.and(
        builder.equal(root.get(ID_PROPERTY_NAME), entity.getId()),
        builder.equal(root.get("version"), entity.getVersion())
    ));

    beforeDelete(entity.getId(), entity.getVersion(), root, removeCmd);

    em.createQuery(removeCmd).executeUpdate();

  }

  @Override
  public void delete(Iterable<? extends E> entities) {
    for (E i : entities) {
      delete(i);
    }
  }

  @Override
  public void deleteAll() {
    final CriteriaBuilder builder = em.getCriteriaBuilder();
    final CriteriaDelete<E> del = builder.createCriteriaDelete(entityClass);

    final Root<E> root = del.from(entityClass);

    beforeDeleteAll(root, del);
    em.createQuery(del).executeUpdate();
  }

  @Transactional
  @Override
  public void deleteAll(Iterable<E> entities) {
    for (E e : entities) {
      em.remove(e);
    }
  }


  protected void beforeDelete(K id, long version, Root<E> root, CriteriaDelete<E> criteria) {
  }

  protected void beforeDeleteAll(Root<E> root, CriteriaDelete<E> criteria) {
  }

  protected <S extends E> void beforeCreate(S entity) {
  }

  protected <S extends E> void beforeUpdate(S entity, E oldValue) {
  }

}
