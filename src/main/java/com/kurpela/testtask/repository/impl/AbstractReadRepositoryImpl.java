package com.kurpela.testtask.repository.impl;

import com.kurpela.testtask.entity.Identifiable;
import com.kurpela.testtask.entity.OrderBy;
import com.kurpela.testtask.exception.EntityNotPersistedException;
import com.kurpela.testtask.repository.Repository;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public abstract class AbstractReadRepositoryImpl<E extends Identifiable<K>,
    K extends Serializable>
    implements Repository<E, K> {


  public static final String ID_PROPERTY_NAME = "id";
  protected final EntityManager em;
  protected final Class<E> entityClass;


  protected AbstractReadRepositoryImpl(EntityManager em, Class<E> entityClass) {
    this.em = em;
    this.entityClass = entityClass;
  }

  @SuppressWarnings("EmptyCatchBlock")
  @Override
  public E findOne(K id) {
    E result = null;
    try {
      result = executeQuery((root, b) -> b.equal(root.get(ID_PROPERTY_NAME), id))
          .getSingleResult();
    } catch (NoResultException expected) {
    }

    return result;
  }

  @Override
  public boolean exists(K id) {
    return executeQuery(Long.class,
        (root, b) -> b.count(root),
        (root, b) -> b.equal(root.get(ID_PROPERTY_NAME), id)
    ).getSingleResult() > 0L;
  }

  @Override
  public List<E> findAll(OrderBy... orderByInfo) {
    return executeQueryList(orderByInfo);
  }

  @Override
  public List<E> findAll(Iterable<K> ids, OrderBy... orderByInfo) {
    return executeQueryList((root, b) -> root.in(ids), orderByInfo);
  }

  @Override
  public List<E> findAll(Sort sort) {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public Page<E> findAll(Pageable pageable, OrderBy... orderByInfo) {
    return executePageQuery(pageable, orderByInfo);
  }

  @Override
  public long count() {
    return executeQuery(Long.class, (root, b) -> b.count(root)).getSingleResult();
  }


  protected TypedQuery<E> executeQuery(OrderBy... orderByInfo) {
    return executeQuery((root, b) -> root, (r, b) -> null, orderByInfo);
  }

  protected <R> TypedQuery<R> executeQuery(
      Class<R> resultClass, SelectionFactory<E, R> factory, OrderBy... orderByInfo) {
    return executeQuery(resultClass, factory, (r, b) -> null, orderByInfo);
  }

  protected List<E> executeQueryList(OrderBy... orderByInfo) {
    return executeQuery((r, b) -> null, addDefaultOrderClause(orderByInfo)).getResultList();
  }

  protected List<E> executeQueryList(QueryProcessor<E, E> builder, OrderBy... orderByInfo) {
    return executeQuery(builder, addDefaultOrderClause(orderByInfo)).getResultList();
  }

  protected TypedQuery<E> executeQuery(QueryProcessor<E, E> builder, OrderBy... orderByInfo) {
    return executeQuery((root, b) -> root, builder, orderByInfo);
  }

  protected TypedQuery<E> executeQuery(SelectionFactory<E, E> factory,
                                       QueryProcessor<E, E> b,
                                       OrderBy... orderByInfo) {
    return executeQuery(entityClass, factory, b, orderByInfo);
  }

  protected PageImpl<E> executePageQuery(Pageable pageable, OrderBy... orderByInfo) {
    return executePageQuery(pageable, (root, b) -> root, (r, b) -> b.and(), orderByInfo);
  }

  protected PageImpl<E> executePageQuery(Pageable pageable,
                                         QueryProcessor<E, E> builder,
                                         OrderBy... orderByInfo) {
    return executePageQuery(pageable, (root, b) -> root, builder, orderByInfo);
  }

  protected PageImpl<E> executePageQuery(Pageable pageable,
                                         SelectionFactory<E, E> factory,
                                         QueryProcessor<E, E> builder,
                                         OrderBy... orderByInfo) {
    return executePageQuery(entityClass, pageable, factory, builder, orderByInfo);
  }

  protected <R> PageImpl<R> executePageQuery(Class<R> resultClass,
                                             Pageable pageable,
                                             SelectionFactory<E, R> factory,
                                             QueryProcessor<E, R> builder,
                                             OrderBy... orderByInfo) {


    List<R> list = executeQuery(resultClass, factory, builder, addDefaultOrderClause(orderByInfo))
        .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
        .setMaxResults(pageable.getPageSize())
        .getResultList();

    Long total = executeQuery(Long.class,
        (root, b) -> b.count(root), ((root, b) -> b.and(builder.process(root, b))))
        .getSingleResult();

    return new PageImpl<>(list, pageable, total);
  }

  protected <R> TypedQuery<R> executeQuery(
      Class<R> resultClass,
      SelectionFactory<E, R> factory,
      QueryProcessor<E, R> builder,
      OrderBy... orderByInfo) {
    return executeQuery(resultClass, factory, builder, false, orderByInfo);
  }

  protected <R> TypedQuery<R> executeQuery(
      Class<R> resultClass,
      SelectionFactory<E, R> factory,
      QueryProcessor<E, R> builder,
      boolean distinct,
      OrderBy... orderByInfo) {


    final CriteriaBuilder b = em.getCriteriaBuilder();
    CriteriaQuery<R> criteria = b.createQuery(resultClass);
    criteria = criteria.distinct(distinct);
    final Root<E> root = criteria.from(entityClass);
    Predicate predicate = builder.process(root, b);
    criteria.select(factory.create(root, b));

    List<OrderBy> order = Arrays.asList(orderByInfo);

    List<javax.persistence.criteria.Order> ord = new ArrayList<>();
    for (OrderBy o : order) {
      Path<Object> propName = root.get(o.getPropName());
      if(o.isAsc()) {
        ord.add(b.asc(propName));
      } {
        ord.add(b.desc(propName));
      }
    }

    if (!ord.isEmpty()) {
      criteria.orderBy(ord);
    }

    Predicate injectedPredicate = beforeEachQuery(root, b, criteria);


    if (predicate != null && injectedPredicate != null) {
      criteria.where(b.and(predicate, injectedPredicate));
    } else if (predicate != null) {
      criteria.where(predicate);
    } else if (injectedPredicate != null) {
      criteria.where(injectedPredicate);
    }


    return em.createQuery(criteria);
  }

  protected <R> Predicate beforeEachQuery(Root<E> root, CriteriaBuilder b, CriteriaQuery<R> q) {
    return null;
  }

  protected OrderBy[] addDefaultOrderClause(OrderBy... orderByInfo) {
    List<OrderBy> orderByList = new ArrayList<>(Arrays.asList(orderByInfo));

    OrderBy[] newArray = new OrderBy[orderByList.size()];
    orderByList.toArray(newArray);
    return newArray;
  }

  protected void ensureEntityIsNotNew(E entity) {
    if (entity.isNew()) {
      throw new EntityNotPersistedException();
    }
  }

  @FunctionalInterface
  public interface QueryProcessor<R, Q> {
    Predicate process(Root<R> root, CriteriaBuilder builder);
  }

  @FunctionalInterface
  protected interface SelectionFactory<R, Q> {
    Expression<Q> create(Root<R> root, CriteriaBuilder builder);
  }

}
