package com.kurpela.testtask.repository.impl;

import com.kurpela.testtask.entity.User;
import com.kurpela.testtask.repository.UserRepository;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl
    extends AbstractCrudRepositoryImpl<User, String>
    implements UserRepository {

  public UserRepositoryImpl(EntityManager em) {
    super(em, User.class);
  }
}
