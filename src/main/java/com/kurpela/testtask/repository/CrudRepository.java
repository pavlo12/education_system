package com.kurpela.testtask.repository;

import com.kurpela.testtask.entity.Identifiable;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CrudRepository<E extends Identifiable<K>, K extends Serializable>
    extends Repository<E, K> {

  <S extends E> S create(S entity);

  <S extends E> List<S> create(Iterable<S> entity);

  <S extends E> S update(S entity);

  <S extends E> List<S> update(Iterable<S> entity);

  void delete(K id);

  void delete(E entity);

  void delete(Iterable<? extends E> entities);

  void deleteAll();

  void deleteAll(Iterable<E> entities);
}
