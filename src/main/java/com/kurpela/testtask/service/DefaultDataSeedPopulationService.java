package com.kurpela.testtask.service;

import org.springframework.transaction.annotation.Transactional;

public interface DefaultDataSeedPopulationService {
  @Transactional
  void populate();
}
