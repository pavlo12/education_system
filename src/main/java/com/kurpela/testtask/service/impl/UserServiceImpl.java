package com.kurpela.testtask.service.impl;

import com.kurpela.testtask.entity.User;
import com.kurpela.testtask.exception.EntityNotPersistedException;
import com.kurpela.testtask.repository.UserRepository;
import com.kurpela.testtask.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends AbstractService<User, String> implements UserService {

  public UserServiceImpl(UserRepository repo,
                         PasswordEncoder passwordEncoder) {
    super(repo);
    this.passwordEncoder = passwordEncoder;
  }

  private PasswordEncoder passwordEncoder;


  public void encodePassword(User user) {
    user.setPassword(passwordEncoder.encode(user.getPassword()));
  }

  @Override
  public User create(User entity) {

    encodePassword(entity);

    return super.create(entity);
  }

  @Override
  public User update(User entity) {

    User user = find(entity.getId());
    if(user == null) {
      throw new EntityNotPersistedException();
    }

    entity.setPassword(user.getPassword());

    return super.update(entity);
  }
}
