package com.kurpela.testtask.service.impl;

import com.kurpela.testtask.entity.Identifiable;
import com.kurpela.testtask.repository.CrudRepository;
import com.kurpela.testtask.service.Service;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractService
    <E extends Identifiable<K>, K extends Serializable>
    implements Service<E, K> {
  protected final CrudRepository<E, K> repo;
  public AbstractService(CrudRepository<E, K> repo) {
    this.repo = repo;
  }


  @Override
  public E find(final K key) {
    return repo.findOne(key);
  }

  @Override
  public List<E> findAll(final Iterable<K> ids) {
    return repo.findAll(ids);
  }

  @Override
  public List<E> findAll() {
    return repo.findAll();
  }

  @Override
  public Page<E> findAll(final Pageable pageable) {
    return repo.findAll(pageable);
  }

  @Override
  @Transactional
  public E create(final E entity) {
    return repo.create(entity);
  }

  @Override
  @Transactional
  public List<E> create(final Iterable<E> entityList) {
    return repo.create(entityList);
  }

  @Override
  @Transactional
  public E update(final E entity) {
    return repo.update(entity);
  }

  @Override
  @Transactional
  public List<E> update(final Iterable<E> entityList) {
    return repo.update(entityList);
  }

  @Override
  @Transactional
  public void delete(final K key) {
    repo.delete(key);
  }

  @Override
  @Transactional
  public void delete(final E entity) {
    repo.delete(entity);
  }

  @Override
  @Transactional
  public void deleteAll(Iterable<E> permissions) {
    repo.deleteAll(permissions);
  }

}
