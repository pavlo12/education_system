package com.kurpela.testtask.service.impl;


import com.kurpela.testtask.entity.Sex;
import com.kurpela.testtask.entity.User;
import com.kurpela.testtask.service.DefaultDataSeedPopulationService;
import com.kurpela.testtask.service.UserService;
import java.time.LocalDate;
import java.util.Collections;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Primary
public class DefaultDataSeedPopulationServiceImpl implements DefaultDataSeedPopulationService {

  private final UserService userService;

  public DefaultDataSeedPopulationServiceImpl(UserService userService) {
    this.userService = userService;
  }

  private User createUser(String login) {
    User user = userService.find(login);

    if(user == null) {
      user = new User();
      user.setId(login);
      user.setFirstName("admin");
      user.setLastName("admin");
      user.setMiddleName("admin");
      user.setEmail("admin@gmail.com");
      user.setBirthDate(LocalDate.of(1992, 9, 12));
      user.setPassword("admin");
      user.setSex(Sex.MAN);
      user.setRoles(Collections.singleton("ADMIN"));
      userService.create(user);
    }
    return user;
  }


  @Override
  @Transactional
  public void populate() {
    createUser("admin");
  }
}
