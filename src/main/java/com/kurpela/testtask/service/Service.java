package com.kurpela.testtask.service;

import com.kurpela.testtask.entity.Identifiable;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface Service<E extends Identifiable<K>, K extends Serializable> {
  E find(K key);

  List<E> findAll(Iterable<K> ids);

  List<E> findAll();

  Page<E> findAll(Pageable pageable);

  E create(E entity);

  List<E> create(Iterable<E> entityList);

  E update(E entity);

  List<E> update(Iterable<E> entityList);

  void delete(K key);
  void delete(E key);

  void deleteAll(Iterable<E> permissions);
}
