package com.kurpela.testtask.service;

import com.kurpela.testtask.entity.User;

public interface UserService extends Service<User, String> {
}
