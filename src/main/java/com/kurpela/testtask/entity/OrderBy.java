package com.kurpela.testtask.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
//@AllArgsConstructor
public class OrderBy
{
  private String propName;
  private boolean isAsc;

  public OrderBy() {
  }

  public OrderBy(String propName, boolean isAsc) {
    this.propName = propName;
    this.isAsc = isAsc;
  }
}