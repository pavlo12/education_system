package com.kurpela.testtask.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@MappedSuperclass
public abstract class Identifiable<C extends Serializable> {
  @Version
  @Column(name = "version", nullable = false)
  private Long version = null;
  @Id
  private C id;

  @JsonIgnore
  public boolean isNew() {
    return version == null;
  }
}
