package com.kurpela.testtask.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kurpela.testtask.serialization.JsonLocalDateDeserializer;
import com.kurpela.testtask.serialization.JsonLocalDateSerializer;
import java.time.LocalDate;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User extends Identifiable<String> {

  @Column(name = "email", nullable = false)
  private String email;
  @Column(name = "first_name", nullable = false)
  private String firstName;
  @Column(name = "middle_name")
  private String middleName;
  @Column(name = "last_name", nullable = false)
  private String lastName;
  @Column(name = "photo")
  private String photo;
  @JsonDeserialize(using = JsonLocalDateDeserializer.class)
  @JsonSerialize(using = JsonLocalDateSerializer.class)
  @Column(name = "birth_date", nullable = false)
  private LocalDate birthDate;
  @Enumerated(EnumType.STRING)
  @Column(name = "sex")
  private Sex sex;
//  @JsonIgnore
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @Column(name = "password", nullable = false)
  private String password;

  @JsonIgnore
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "role_users")
  private Set<String> roles;

//  @JsonIgnore
//  public String getPassword() {
//    return password;
//  }
//
//
//  public void setPassword(String password) {
//    this.password = password;
//  }
}
